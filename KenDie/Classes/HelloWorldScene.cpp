#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

/*

	全局音乐函数

*/

bool getMusiState()
{
	return (UserDefault::getInstance()->getBoolForKey("isMusic"));
}

void setMusi(bool state)//设置音乐状态
{
	UserDefault::getInstance()->setBoolForKey("isMusic", state);
	if (state == true)
	{
		SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1);
		SimpleAudioEngine::getInstance()->setEffectsVolume(1);
	}
	else
	{

		SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0);
		SimpleAudioEngine::getInstance()->setEffectsVolume(0);
	}

}











Scene* logo::createScene()
{
	auto scene = Scene::create();
	auto layer = logo::create();
	scene->addChild(layer);
	return scene;
}


bool logo::init()
{
	Layer::init();

	auto bg = Sprite::create("logo1.png");
	bg->setPosition(1136 / 2, 640 / 2);
	this->addChild(bg);
	bg->setOpacity(0);
	bg->runAction(FadeIn::create(0.4));
	this->runAction(Sequence::create(DelayTime::create(4), CallFunc::create([=]{
		Director::getInstance()->replaceScene(notify::createScene());


	}), nullptr));

	return true;
}







Scene* notify::createScene()
{
	auto scene = Scene::create();
	auto layer = notify::create();
	scene->addChild(layer);
	return scene;
}


bool notify::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("UIResoure2.plist");

	auto dic = Dictionary::createWithContentsOfFile("string.plist");


	bbb = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(notify::cal));
	bbb->setPosition(Vec2(1136 / 2, 640 / 2));
	//bbb->setOpacity(99);

	auto menuu = Menu::create(bbb, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);


	label = Label::createWithTTF(((String*)dic->objectForKey("load"))->getCString(), "fzjdjw_ziku.ttf", 45);
	label->setPosition(1136 / 2, 640 / 2);
	bbb->addChild(label);



	 back_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("all-pass-home-1.png"),
		Sprite::createWithSpriteFrameName("all-pass-home-2.png"), NULL, this, menu_selector(notify::call1)
		);
	back_1->setPosition(Vec2(1136 / 2 + 500, 100));
	auto back = Menu::create(back_1, nullptr);
	back->setPosition(Point::ZERO);
	bbb->addChild(back);


	back_1->setVisible(false);

	
	this->runAction(Sequence::create(DelayTime::create(3), CallFunc::create([=]{

		ws_client = new cocos2d::network::WebSocket();
		ws_client->init(*this, "ws://qq771911064.oicp.net:3011");
		ws_client->send("765767");

	
	
	}), nullptr));

	return true;
}




void notify::call1(Ref*p)
{
	Director::getInstance()->replaceScene(HelloWorld::createScene());
}



























Scene* HelloWorld::createScene()
{
	auto scene = Scene::create();
	auto layer = HelloWorld::create();
	scene->addChild(layer);
	return scene;
}


bool HelloWorld::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("UIResoure2.plist");




	auto sp = Sprite::createWithSpriteFrameName("start-bg.png");
	sp->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(sp);
	auto menu_start_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("play-1.png"),
		Sprite::createWithSpriteFrameName("play-2.png"), NULL, this, menu_selector(HelloWorld::call)
		);
	menu_start_1->setPosition(Vec2(1136 / 2, 640 / 2 - 30));
	auto menu_start = Menu::create(menu_start_1, nullptr);
	menu_start->setPosition(Point::ZERO);
	this->addChild(menu_start);



	auto men_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("information-1.png"),
		Sprite::createWithSpriteFrameName("information-2.png"), NULL, this, menu_selector(HelloWorld::call_info)
		);
	men_1->setPosition(Vec2(1136 / 2 + 150 + 100, 50));
	auto men1 = Menu::create(men_1, nullptr);
	men1->setPosition(Point::ZERO);
	this->addChild(men1);


	auto men_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("comment-1.png"),
		Sprite::createWithSpriteFrameName("comment-2.png"), NULL, this, menu_selector(HelloWorld::call_good)
		);
	men_11->setPosition(Vec2(1136 / 2 + 150 + 120 + 100, 50));
	auto men11 = Menu::create(men_11, nullptr);
	men11->setPosition(Point::ZERO);
	this->addChild(men11);


	auto men_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("feedback-1.png"),
		Sprite::createWithSpriteFrameName("feedback-2.png"), NULL, this, menu_selector(HelloWorld::call_review)
		);
	men_111->setPosition(Vec2(1136 / 2 + 150 + 120 + 120 + 100, 50));
	auto men111 = Menu::create(men_111, nullptr);
	men111->setPosition(Point::ZERO);
	this->addChild(men111);

	auto dic = Dictionary::createWithContentsOfFile("string.plist");

	auto s = (String*)dic->objectForKey("name");
	LabelTTF*lab1 = LabelTTF::create(s->getCString(), "Arial", 30);
	lab1->setPosition(Vec2(1136 / 2 - 440, 50));
	lab1->setColor(Color3B(0, 130, 44));
	this->addChild(lab1);




	musi_of = MenuItemSprite::create(Sprite::createWithSpriteFrameName("music.png"),
		Sprite::createWithSpriteFrameName("music.png"), NULL, this, menu_selector(HelloWorld::musi_off)
		);
	musi_of->setPosition(Vec2(1136 / 2 + 490, 170));

	musi_onn = MenuItemSprite::create(Sprite::createWithSpriteFrameName("close-music.png"),
		Sprite::createWithSpriteFrameName("close-music.png"), NULL, this, menu_selector(HelloWorld::musi_on)
		);
	musi_onn->setPosition(Vec2(1136 / 2 + 490, 170));

	auto mm = Menu::create(musi_of, musi_onn, nullptr);
	mm->setPosition(0, 0);
	this->addChild(mm);


	setMusi(getMusiState());

	musi_onn->setVisible(!getMusiState());

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("catch_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("catch_BG.mp3",true);

	/*
	bbb = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(HelloWorld::cal));
	bbb->setPosition(Vec2(1136 / 2, 640 / 2));
	//bbb->setOpacity(99);

	auto menuu = Menu::create(bbb, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	
	label = Label::createWithTTF( ((String*)dic->objectForKey("load"))->getCString() , "fzjdjw_ziku.ttf", 45);
	label->setPosition(1136 / 2, 640 / 2 );
	bbb->addChild(label);



	auto back_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("all-pass-home-1.png"),
		Sprite::createWithSpriteFrameName("all-pass-home-2.png"), NULL, this, menu_selector(HelloWorld::call1)
		);
	back_1->setPosition(Vec2(1136 / 2+500, 100));
	auto back = Menu::create(back_1, nullptr);
	back->setPosition(Point::ZERO);
	bbb->addChild(back);




	ws_client = new cocos2d::network::WebSocket();
	ws_client->init(*this, "ws://localhost:3011");
	ws_client->send("765767");

	*/

	return true;
}

void HelloWorld::call(Ref* p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");
	Director::getInstance()->replaceScene(selectScene::createScene());

}


void HelloWorld::musi_off(Ref*p)
{
	setMusi(false);
	musi_onn->setVisible(true);
}

void HelloWorld::musi_on(Ref*p)
{
	setMusi(true);
	musi_onn->setVisible(false);

}


void HelloWorld::call_info(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");


}

void HelloWorld::call_good(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");
	/*ws_client = new cocos2d::network::WebSocket();
	ws_client->init(*this, "ws://localhost:3011");
	ws_client->send("765767");*/
	

}


void HelloWorld::call_review(Ref*p)
{

	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

}




Scene* selectScene::createScene()
{
	auto scene = Scene::create();
	auto layer1 = selectScene::create();
	scene->addChild(layer1);
	return scene;
}


bool selectScene::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("UIResoure2.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("level_scene.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("level_image.plist");

	auto bg = Sprite::createWithSpriteFrameName("list-bg.png");
	this->addChild(bg);
	bg->setPosition(1136 / 2, 640 / 2);

	auto back_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("back-1.png"),
		Sprite::createWithSpriteFrameName("back-2.png"), NULL, this, menu_selector(selectScene::call)
		);
	back_1->setPosition(Vec2(1136 / 2 - 500, 50));
	auto back = Menu::create(back_1, nullptr);
	back->setPosition(Point::ZERO);
	this->addChild(back);


	for (int i = 0; i < 6; i++)
	{
		layer[i] = Layer::create();
		layer[i]->setTag(i);

		layer[i]->setVisible(false);
		this->addChild(layer[i]);

	}


	Sprite *star1[6];
	for (int x = 0; x < 6; x++)
	{
		star[x] = Sprite::createWithSpriteFrameName("dots-1.png");
		this->addChild(star[x]);
		star[x]->setPosition(1136 / 2 - 120 + x * 40, 640 / 2 - 200);


	}


	for (int x = 0; x < 6; x++)
	{
		star[x] = Sprite::createWithSpriteFrameName("dots-2.png");
		this->addChild(star[x]);
		star[x]->setPosition(1136 / 2 - 120 + x * 40, 640 / 2 - 200);

		star[x]->setVisible(false);
	}


	//// 1
	layer[0]->setVisible(true);

	item[0] = Sprite::createWithSpriteFrameName("shutdown-pc-cover.png");
	item[0]->setPosition(1136 / 2 - 150, 640 / 2 + 170);
	layer[0]->addChild(item[0]);

	item[1] = Sprite::createWithSpriteFrameName("clickon-the-100times-cover.png");
	item[1]->setPosition(1136 / 2 + 150, 640 / 2 + 170);
	layer[0]->addChild(item[1]);

	item[2] = Sprite::createWithSpriteFrameName("through-the-bridge-cover.png");
	item[2]->setPosition(1136 / 2 - 150, 640 / 2 - 30);
	layer[0]->addChild(item[2]);

	item[3] = Sprite::createWithSpriteFrameName("click-me-cover.png");
	item[3]->setPosition(1136 / 2 + 150, 640 / 2 - 30);
	layer[0]->addChild(item[3]);




	//// 2
	layer[1]->setVisible(true);

	item[4] = Sprite::createWithSpriteFrameName("play-the-plane-cover.png");
	item[4]->setPosition(1136 / 2 - 150, 640 / 2 + 170);
	layer[1]->addChild(item[4]);

	item[5] = Sprite::createWithSpriteFrameName("cut-rope-cover.png");
	item[5]->setPosition(1136 / 2 + 150, 640 / 2 + 170);
	layer[1]->addChild(item[5]);

	item[6] = Sprite::createWithSpriteFrameName("drive-cover.png");
	item[6]->setPosition(1136 / 2 - 150, 640 / 2 - 30);
	layer[1]->addChild(item[6]);

	item[7] = Sprite::createWithSpriteFrameName("cliff-cover.png");
	item[7]->setPosition(1136 / 2 + 150, 640 / 2 - 30);
	layer[1]->addChild(item[7]);




	//// 3
	layer[2]->setVisible(true);

	item[8] = Sprite::createWithSpriteFrameName("catch-mouse-cover.png");
	item[8]->setPosition(1136 / 2 - 150, 640 / 2 + 170);
	layer[2]->addChild(item[8]);

	item[9] = Sprite::createWithSpriteFrameName("fight-for-princess-cover.png");
	item[9]->setPosition(1136 / 2 + 150, 640 / 2 + 170);
	layer[2]->addChild(item[9]);

	item[10] = Sprite::createWithSpriteFrameName("carracing-cover.png");
	item[10]->setPosition(1136 / 2 - 150, 640 / 2 - 30);
	layer[2]->addChild(item[10]);

	item[11] = Sprite::createWithSpriteFrameName("sprite-afraid-sun-shine-cover.png");
	item[11]->setPosition(1136 / 2 + 150, 640 / 2 - 30);
	layer[2]->addChild(item[11]);




	//// 4
	layer[3]->setVisible(true);

	item[12] = Sprite::createWithSpriteFrameName("run-after-cover.png");
	item[12]->setPosition(1136 / 2 - 150, 640 / 2 + 170);
	layer[3]->addChild(item[12]);

	item[13] = Sprite::createWithSpriteFrameName("boating-cover.png");
	item[13]->setPosition(1136 / 2 + 150, 640 / 2 + 170);
	layer[3]->addChild(item[13]);

	item[14] = Sprite::createWithSpriteFrameName("bad-weather-cover.png");
	item[14]->setPosition(1136 / 2 - 150, 640 / 2 - 30);
	layer[3]->addChild(item[14]);

	item[15] = Sprite::createWithSpriteFrameName("input-password-cover.png");
	item[15]->setPosition(1136 / 2 + 150, 640 / 2 - 30);
	layer[3]->addChild(item[15]);



	//// 5
	layer[4]->setVisible(true);

	item[16] = Sprite::createWithSpriteFrameName("break-bubble-cover.png");
	item[16]->setPosition(1136 / 2 - 150, 640 / 2 + 170);
	layer[4]->addChild(item[16]);

	item[17] = Sprite::createWithSpriteFrameName("send-he-packing-cover.png");
	item[17]->setPosition(1136 / 2 + 150, 640 / 2 + 170);
	layer[4]->addChild(item[17]);

	item[18] = Sprite::createWithSpriteFrameName("now-time-cover.png");
	item[18]->setPosition(1136 / 2 - 150, 640 / 2 - 30);
	layer[4]->addChild(item[18]);

	item[19] = Sprite::createWithSpriteFrameName("pk-with-knife-cover.png");
	item[19]->setPosition(1136 / 2 + 150, 640 / 2 - 30);
	layer[4]->addChild(item[19]);



	//// 6
	layer[5]->setVisible(true);

	item[20] = Sprite::createWithSpriteFrameName("car-passby-road-difficult-cover.png");
	item[20]->setPosition(1136 / 2 - 150, 640 / 2 + 170);
	layer[5]->addChild(item[20]);

	item[21] = Sprite::createWithSpriteFrameName("finger-guess-cover.png");
	item[21]->setPosition(1136 / 2 + 150, 640 / 2 + 170);
	layer[5]->addChild(item[21]);

	item[22] = Sprite::createWithSpriteFrameName("jump-cover.png");
	item[22]->setPosition(1136 / 2 - 150, 640 / 2 - 30);
	layer[5]->addChild(item[22]);

	item[23] = Sprite::createWithSpriteFrameName("coming-soon.png");
	item[23]->setPosition(1136 / 2 + 150, 640 / 2 - 30);
	layer[5]->addChild(item[23]);
	item[23]->setScale(0.5);

	pot[0].x = 418;
	pot[0].y = 490;

	pot[1].x = 718;
	pot[1].y = 490;

	pot[2].x = 418;
	pot[2].y = 290;

	pot[3].x = 718;
	pot[3].y = 290;

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		beg = target->convertToNodeSpace(touch->getLocation());
		nowp.x = beg.x;
		nowp.y = beg.y;
		return true;
	};

	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		if (point.x > 50 && point.x < (1136 - 100))
		{
			/*layer[getVisibleLayer()]->setPositionX(
				layer[getVisibleLayer()]->getPositionX() - (beg.x - point.x));
			beg.x = point.x;
			*/


			layer[getVisibleLayer()]->setPositionX(layer[getVisibleLayer()]->getPositionX() - (nowp.x-point.x));

			nowp.x = point.x;

		}
		return;
	};

	listener->onTouchEnded = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		log("%f", layer[getVisibleLayer()]->getPositionX());

		if (layer[getVisibleLayer()]->getPositionX() > -200 &&
			layer[getVisibleLayer()]->getPositionX() < 200
			)
		{

			layer[getVisibleLayer()]->runAction(MoveTo::create(0.1, Vec2(0, 0)));
		}
		else
		{

			if (layer[getVisibleLayer()]->getPositionX() <= -100)/*lef*/
			{

				if (getVisibleLayer() <= 4)
				{
					layer[getVisibleLayer()]->runAction(MoveTo::create(0.2, Vec2(-1136, 0)));
					setLayerVis(getVisibleLayer() + 1);
					layer[getVisibleLayer()]->setPositionX(1136);
					layer[getVisibleLayer()]->runAction(MoveTo::create(0.2, Vec2(0, 0)));
				}
				else
				{
					layer[getVisibleLayer()]->runAction(MoveTo::create(0.5, Vec2(0, 0)));
				}

			}
			else
			{
				if (getVisibleLayer() >= 1)
				{
				
					layer[getVisibleLayer()]->runAction(MoveTo::create(0.2, Vec2(1136, 0)));
					setLayerVis(getVisibleLayer() -1);
					layer[getVisibleLayer()]->setPositionX(-1136);
					layer[getVisibleLayer()]->runAction(MoveTo::create(0.2, Vec2(0, 0)));


				}
				else
				{
					layer[getVisibleLayer()]->runAction(MoveTo::create(0.5, Vec2(0, 0)));
				}
			}

		}


		log("%f,%f,", beg.x, point.x);
		if (abs(beg.x -point.x)<10 && abs(beg.y - point.y)<10)//点击
		{
			int a = 0;
			for (a = 0; a < 4; a++)
			{

				if ((abs(point.x - pot[a].x) < 123) && (abs(point.y - pot[a].y) < 83))
				{
					// a是点中的编号

					SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");
	
					switch (getVisibleLayer())
					{
					case  0:
					{
							   switch (a)
							   {
							   case 0:
								   Director::getInstance()->replaceScene(scene_1::createScene());
								   break;
							   case 1:
								   Director::getInstance()->replaceScene(scene_2::createScene());
								   break;
							   case 2:
								   Director::getInstance()->replaceScene(scene_3::createScene());
								   break;
							   case 3:
								   Director::getInstance()->replaceScene(scene_4::createScene());
								   break;

							   }
					}
						break;

					case  1:
					{
							   switch (a)
							   {
							   case 0:
								   Director::getInstance()->replaceScene(scene_5::createScene());
								   break;
							   case 1:
								   Director::getInstance()->replaceScene(scene_6::createScene());
								   break;
							   case 2:
								   Director::getInstance()->replaceScene(scene_7::createScene());
								   break;
							   case 3:
								   Director::getInstance()->replaceScene(scene_8::createScene());
								   break;

							   }
					}
						break;


					case  2:
					{
							   switch (a)
							   {
							   case 0:
								   Director::getInstance()->replaceScene(scene_9::createScene());
								   break;
							   case 1:
								   Director::getInstance()->replaceScene(scene_10::createScene());
								   break;
							   case 2:
								   Director::getInstance()->replaceScene(scene_11::createScene());
								   break;
							   case 3:
								   Director::getInstance()->replaceScene(scene_12::createScene());
								   break;

							   }
					}
						break;


					case  3:
					{
							   switch (a)
							   {
							   case 0:
								   Director::getInstance()->replaceScene(scene_13::createScene());
								   break;
							   case 1:
								   Director::getInstance()->replaceScene(scene_14::createScene());
								   break;
							   case 2:
								   Director::getInstance()->replaceScene(scene_15::createScene());
								   break;
							   case 3:
								   Director::getInstance()->replaceScene(scene_16::createScene());
								   break;

							   }
					}
						break;




					case  4://第5个layer
					{
								switch (a)
								{
								case 0:
									Director::getInstance()->replaceScene(scene_17::createScene());
									break;
								case 1:
									Director::getInstance()->replaceScene(scene_18::createScene());
									break;
								case 2:
									Director::getInstance()->replaceScene(scene_19::createScene());
									break;
								case 3:
									Director::getInstance()->replaceScene(scene_20::createScene());
									break;

								}
					}
						break;

					case  5:
					{
							   switch (a)
							   {
							   case 0:
								   Director::getInstance()->replaceScene(scene_21::createScene());
								   break;
							   case 1:
								   Director::getInstance()->replaceScene(scene_22::createScene());
								   break;
							   case 2:
								   Director::getInstance()->replaceScene(scene_23::createScene());
								   break;
							   case 3:
								   return;
								   break;

							   }
					}
						break;

					}
				}

			}
		}
		return;
	};


	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	setLayerVis(UserDefault::getInstance()->getIntegerForKey("selectLayer"));


	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("sound_bg_1.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("sound_bg_1.mp3", true);


	return true;
}


void selectScene::call(Ref* p)
{

	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(HelloWorld::createScene());

}





publicMenu*publicMenu::createNew()
{
	publicMenu*_publicMenu = new publicMenu();
	return _publicMenu;

}

publicMenu::publicMenu()
{
	Layer::init();
	//pause

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("UIResoure2.plist");


	auto menu_pause_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"), NULL, this, menu_selector(publicMenu::call_select)
		);
	menu_pause_1->setPosition(Vec2(1136 / 2 + 510, 600));
	auto menu_pause = Menu::create(menu_pause_1, nullptr);
	menu_pause->setPosition(Point::ZERO);
	this->addChild(menu_pause);
	menu_pause_1->setScale(0.7);


	auto men = Menu::create(menu_pause_1, nullptr);
	men->setPosition(Point::ZERO);
	this->addChild(men);




	Sprite*b1 = Sprite::createWithSpriteFrameName("pass-100-money-2.png");
	//this->addChild(b1);
	b1->setPosition(1136 / 2-250, 600);



	Sprite*b11 = Sprite::createWithSpriteFrameName("tip-20-money-2.png");
	//this->addChild(b11);
	b11->setPosition(1136 / 2-450, 600);



}





void publicMenu::call_select(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());
}








/*start of each game scene
*/


Scene*scene_1::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_1::create();
	scene->addChild(layer);
	return scene;

}


bool scene_1::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("shutdown.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("UIResoure2.plist");


	///背景
	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	//电线

	auto sp1 = Sprite::createWithSpriteFrameName("wire.png");
	sp1->setPosition(Vec2(1136 / 2 - 50, 640 / 2));

	this->addChild(sp1);
	sp1->setRotation(-180);
	sp1->setFlipX(true);
	sp1->setFlipY(true);




	//电脑
	auto conp = Sprite::createWithSpriteFrameName("computer.png");
	conp->setPosition(Vec2(1136 / 2, 640 / 2));

	//电脑动画
	sta_1 = Sprite::createWithSpriteFrameName("computerclose_05.png");
	this->addChild(sta_1);
	sta_1->setPosition(Vec2(1136 / 2 - 200, 640 / 2 + 60));

	auto ani = Animation::create();
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("code_01.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("code_02.png"));
	ani->setDelayPerUnit(2.8 / 10.0);
	ani->setLoops(100);
	ani->setRestoreOriginalFrame(true);
	sta_1->runAction(RepeatForever::create(Animate::create(ani)));


	this->addChild(conp);


	hero = Sprite::createWithSpriteFrameName("hero_01.png");
	hero->setPosition(Vec2(1136 / 2 + 200, 640 / 2));
	this->addChild(hero);

	auto ani11 = Animation::create();
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_02.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_03.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_04.png"));


	//	auto s1=Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_03.png"));

	//ani11->addSpriteFrame((SpriteFrame*)s1);

	//Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_04.png"));


	ani11->setDelayPerUnit(2.8 / 8.0);
	ani11->setLoops(100);
	ani11->setRestoreOriginalFrame(true);
	hero->runAction(RepeatForever::create(Animate::create(ani11)));

	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(false);

	listener1->onTouchBegan = [=](Touch* touch, Event* event){
		auto target = static_cast<Sprite*>(event->getCurrentTarget());

		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);

		if (rect.containsPoint(locationInNode))
		{

			if (locationInNode.x > 333 && locationInNode.x < 360)
			{
				if (locationInNode.y > 370 && locationInNode.y < 415)
				{

					sta_1->stopAllActions();
					sta_1->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("computerclose_05.png"));

					auto ani = Animation::create();
					SimpleAudioEngine::getInstance()->playEffect("shutdownPC_close.mp3");
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("computerclose_01.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("computerclose_02.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("computerclose_03.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("computerclose_04.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("computerclose_05.png"));


					ani->setDelayPerUnit(2.8 / 40);
					ani->setLoops(1);
					ani->setRestoreOriginalFrame(true);

					bg->runAction(Sequence::create(
						CallFunc::create([=]{
						sta_1->runAction(Animate::create(ani)); })
							, DelayTime::create(0.4),
							CallFunc::create([=]{
							auto ani1 = Animation::create();
							SimpleAudioEngine::getInstance()->playEffect("jian-xiao.mp3");
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("herowin_01.png"));
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("herowin_02.png"));
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("herowin_03.png"));
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("herowin_04.png"));
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("herowin_05.png"));
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("herowin_06.png"));

							ani1->setDelayPerUnit(2.8 / 30);
							ani1->setLoops(2);
							ani1->setRestoreOriginalFrame(false);
							hero->stopAllActions();
							hero->runAction(Animate::create(ani1)); })
								,
								DelayTime::create(0.56 * 2),
								CallFunc::create([=]{

								showWin();
							})

								, nullptr));

				}
			}

			if (locationInNode.x > 500 && locationInNode.x < 700)
			{
				if (locationInNode.y > 270 && locationInNode.y < 430)
				{
					hero->stopAllActions();
					hero->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_11.png"));
					auto ani = Animation::create();
					SimpleAudioEngine::getInstance()->playEffect("dian-ji.mp3");

					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_05.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_06.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_07.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_08.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_09.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_10.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_06.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_07.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_08.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_09.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_10.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_09.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_10.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_09.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_10.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_09.png"));
					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_10.png"));


					ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_11.png"));


					ani->setDelayPerUnit(2.8 / 40);
					ani->setLoops(1);
					ani->setRestoreOriginalFrame(true);

					bg->runAction(Sequence::create(
						CallFunc::create([=]{
						hero->runAction(Animate::create(ani)); })
							, DelayTime::create(16 * (4.0 / 40)),

							CallFunc::create([=]{
							showLose();
						})

							, nullptr));


				}
			}



			return true;
		}
		return false;
	};




	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, conp);
	_eventDispatcher->setEnabled(false);


	auto t1 = Sprite::createWithSpriteFrameName("word_01.png");
	t1->setPosition(Vec2(1136 / 2 - 100, 640 / 2 + 200));
	this->addChild(t1);


	auto t11 = Sprite::createWithSpriteFrameName("word_02.png");
	t11->setPosition(Vec2(1136 / 2 + 80, 640 / 2 + 200));
	this->addChild(t11);


	auto t111 = Sprite::createWithSpriteFrameName("word_03.png");
	t111->setPosition(Vec2(1136 / 2 + 180 + 80, 640 / 2 + 200));
	this->addChild(t111);


	t1->setOpacity(0);
	t11->setOpacity(0);
	t111->setOpacity(0);

	auto text = Sprite::createWithSpriteFrameName("text.png");
	text->setPosition(Vec2(1136 / 2 + 300, 640 / 2 - 200));
	this->addChild(text);
	text->setFlippedY(true);

	auto text1 = Sprite::create("1.png");
	text1->setPosition(Vec2(1136 / 2 + 320, 640 / 2 - 226));
	this->addChild(text1);
	text1->setOpacity(0);
	text->setOpacity(0);

	this->runAction(Sequence::create(
		CallFunc::create([=]{t1->runAction(FadeIn::create(2.0)); }), DelayTime::create(2),
		CallFunc::create([=]{t11->runAction(FadeIn::create(2.0)); }), DelayTime::create(2),
		CallFunc::create([=]{t111->runAction(FadeIn::create(2.0)); }), DelayTime::create(2.0),
		CallFunc::create([=]{
		text->runAction(FadeIn::create(0.5));
		text1->runAction(FadeIn::create(0.5));
		_eventDispatcher->setEnabled(true);

		auto sta = Sprite::createWithSpriteFrameName("star_03.png");
		sta->setPosition(Vec2(1136 / 2 - 50, 640 / 2 + 30));
		this->addChild(sta);


		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("star_01.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("star_02.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("star_03.png"));
		ani1->setDelayPerUnit(2.8 / 10.0);
		ani1->setLoops(100);
		ani1->setRestoreOriginalFrame(true);
		sta->runAction(RepeatForever::create(Animate::create(ani1)));




	}),
		DelayTime::create(5.0),
		CallFunc::create([=]{
		text->runAction(FadeOut::create(0.5));
		text1->runAction(FadeOut::create(0.5));
	})
		, nullptr
		));

	this->addChild(publicMenu::createNew());


	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("shu-huan.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("shu-huan.mp3", true);





	return true;
}



////


void scene_1::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0001.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_1::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_1::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);





	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_1::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_1::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");


	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0002.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_1::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_1::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}






void scene_1::call_1(Ref*p)
{

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("catch_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("catch_BG.mp3", true);


	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());


}

void scene_1::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");


	Director::getInstance()->replaceScene(scene_1::createScene());
}

void scene_1::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");


	Director::getInstance()->replaceScene(scene_2::createScene());

}







Scene*scene_2::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_2::create();
	scene->addChild(layer);
	return scene;

}


bool scene_2::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();


	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("click_on_the_100_times.plist");

	bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);



	menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_01.png"),
		Sprite::createWithSpriteFrameName("button_02.png"),
		nullptr
		, this, menu_selector(scene_2::call_red));

	menu_1->setPosition(Vec2(1136 / 2 - 200, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_03.png"),
		Sprite::createWithSpriteFrameName("button_04.png"),
		nullptr
		, this, menu_selector(scene_2::call_lo));

	menu_11->setPosition(Vec2(1136 / 2 + 200, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



	lab = Label::createWithTTF("100", "fzjdjw_ziku.ttf", 150);
	this->addChild(lab);
	lab->setPosition(Vec2(1136 / 2 + 110, 640 / 2 + 120));
	lab->setColor(Color3B(0, 130, 25));

	this->addChild(publicMenu::createNew());



	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("hua-ji-you-xi-bei-jing.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("hua-ji-you-xi-bei-jing.mp3", true);




	return true;
}



////


void scene_2::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");



	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0004.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_2::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_2::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);





	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_2::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_2::showLose()
{


	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");


	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0003.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_2::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_2::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}






void scene_2::call_1(Ref*p)
{

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("catch_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("catch_BG.mp3", true);

	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());


}

void scene_2::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_2::createScene());
}

void scene_2::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_3::createScene());
}


void scene_2::call_red(Ref*p)
{



	SimpleAudioEngine::getInstance()->playEffect("show.mp3");






	i--;
	char a[10];
	sprintf(a, "%d", i);
	lab->setString(a);
	if (i == 9)
	{
		menu_1->setPosition(Vec2(1136 / 2 + 200, 640 / 2 - 50));
		menu_11->setPosition(Vec2(1136 / 2 - 200, 640 / 2 - 50));
	}

	if (i == 8)
	{
		menu_1->setPosition(Vec2(1136 / 2 - 200, 640 / 2 - 50));
		menu_11->setPosition(Vec2(1136 / 2 + 200, 640 / 2 - 50));
	}

	if (i == 7)
	{
		menu_1->setPosition(Vec2(1136 / 2 + 200, 640 / 2 - 50));
		menu_11->setPosition(Vec2(1136 / 2 - 200, 640 / 2 - 50));
	}

	if (i == 2)
	{
		menu_1->setPosition(Vec2(1136 / 2 - 200, 640 / 2 - 50));
		menu_11->setPosition(Vec2(1136 / 2 + 200, 640 / 2 - 50));
	}

	if (i == 1)
	{
		menu_1->setPosition(Vec2(1136 / 2 + 200, 640 / 2 - 50));
		menu_11->setPosition(Vec2(1136 / 2 - 200, 640 / 2 - 50));
	}

	if (i <= 0)
	{
		showWin();
	}


}

void scene_2::call_lo(Ref*p)
{

	this->runAction(Sequence::create(CallFunc::create(
		[=]{
		ss = Sprite::createWithSpriteFrameName("blow_up_02.png");

		ss->setPosition(Vec2(1136 / 2, 640 / 2));
		this->addChild(ss);

		ss->runAction(Blink::create(1.0, 15));
		SimpleAudioEngine::getInstance()->playEffect("boom.mp3");
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();

		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_02.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_01.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_03.png"));

		ani1->setDelayPerUnit(2.8 / 20);
		ani1->setLoops(2);
		ani1->setRestoreOriginalFrame(true);
		ss->runAction(Animate::create(ani1));
		ss->runAction(Blink::create(1.0, 7));
		menu_11->setEnabled(false);
		menu_1->setEnabled(false);

	}

	), DelayTime::create(1.01),
		CallFunc::create([=]{
		ss->setVisible(false);
		showLose(); })
			, nullptr));

}





Scene*scene_3::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_3::create();
	scene->addChild(layer);
	return scene;

}


bool scene_3::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();


	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("through_the_bridge.plist");

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	bridge = Sprite::createWithSpriteFrameName("bridge-01.png");
	bridge->setPosition(Vec2(1136 / 2, 640 / 2 - 200));
	this->addChild(bridge);



	man = Sprite::createWithSpriteFrameName("hero-car.png");
	man->setPosition(Vec2(-270 / 2 - 25, 640 / 2 + 10));
	this->addChild(man);


	menu_man_sp = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hero-car.png")
		, Sprite::createWithSpriteFrameName("hero-car.png"), nullptr, this, menu_selector(scene_3::call_man_1));
	menu_man_sp->setPosition(Vec2(-270 / 2 - 25, 640 / 2 + 10));
	auto ss = Menu::create(menu_man_sp, nullptr);
	ss->setPosition(Point::ZERO);
	this->addChild(ss);


	mm1 = Sprite::createWithSpriteFrameName("hero-jump-03.png");
	mm1->setPosition(Vec2(-300, -300));
	this->addChild(mm1);



	tank = Sprite::createWithSpriteFrameName("tanks-no-hero.png");
	tank->setPosition(Vec2(-270 / 2, 640 / 2 - 38));
	this->addChild(tank);
	mm1->setScale(0.9);

	auto tmp = Sprite::createWithSpriteFrameName("tanks-no-hero.png");
	tmp->setTextureRect(Rect(2, 1454, 262, 100));
	menu_tank_sp = MenuItemSprite::create(tmp
		, tmp, nullptr, this, menu_selector(scene_3::call_tank));
	menu_tank_sp->setPosition(Vec2(-270 / 2, 640 / 2 - 46));


	auto s = Menu::create(menu_tank_sp, nullptr);
	s->setPosition(Point::ZERO);
	this->addChild(s);


	menu_man_sp->setVisible(true);





	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("tan-ke-jing-guo.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("tan-ke-jing-guo.mp3", true);



	this->addChild(publicMenu::createNew());
	this->scheduleUpdate();
	return true;
}



////


void scene_3::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");


	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0006.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_3::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_3::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);





	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_3::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_3::showLose()
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_3::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0005.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_3::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_3::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);





}






void scene_3::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("catch_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("catch_BG.mp3", true);

	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());


}

void scene_3::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");


	Director::getInstance()->replaceScene(scene_3::createScene());
}

void scene_3::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_4::createScene());
}


void scene_3::update(float delta)
{


	if (menu_tank_sp->isVisible() == true)
		tank->setPositionX(tank->getPositionX() + 1),
		menu_tank_sp->setPositionX(tank->getPositionX());

	if (menu_man_sp->isVisible() == true && menu_tank_sp->isVisible() == true)
		man->setPositionX(man->getPositionX() + 1),
		menu_man_sp->setPositionX(man->getPositionX());




	/*坦克和人 一起掉*/
	if (tank->getPositionX() > 1136 / 2 - 100)
	{
		//桥动画
		menu_man_sp->setVisible(false);
		menu_tank_sp->setVisible(false);
		_eventDispatcher->removeAllEventListeners();
	
		this->unscheduleUpdate();

		if (tagg == true)
		{
			walk->setVisible(true);
			mm->setVisible(false);

			if (tag1 == false)
			{
				mm->setVisible(true);
				walk->setVisible(false);

			}


		}

		bridge->runAction(Sequence::create(
			CallFunc::create([=]{
			auto ani = Animation::create();
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bridge-01.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bridge-02.png"));

			ani->setDelayPerUnit(2.8 / 30);
			ani->setLoops(4);
			ani->setRestoreOriginalFrame(true);
			bridge->runAction(Animate::create(ani));


		}), DelayTime::create(0.7),
			CallFunc::create([=]{
			auto ani1 = Animation::create();

			bridge->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bridge-rupture-05.png"));
			SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			SimpleAudioEngine::getInstance()->playEffect("bridge_screem.mp3");

			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bridge-rupture-01.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bridge-rupture-02.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bridge-rupture-03.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bridge-rupture-04.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bridge-rupture-05.png"));
			ani1->setDelayPerUnit(2.8 / 30);
			ani1->setLoops(1);
			ani1->setRestoreOriginalFrame(true);
			bridge->runAction(Animate::create(ani1));

			man->runAction(RotateBy::create(0.7, -80));
			man->runAction(MoveTo::create(0.7, Vec2(man->getPositionX(), -120)));

			tank->runAction(RotateBy::create(0.7, 70));
			tank->runAction(MoveTo::create(0.7, Vec2(tank->getPositionX(), -120)));

			mm1->setVisible(false);
			//	mm->setVisible(false);

			if (man->isVisible() == false)
			{
				walk->setEnabled(false);
				walk->setVisible(false);
				mm1->setVisible(false);

			


				mm->setVisible(true);
				mm->runAction(RotateBy::create(0.7, -80));
				mm->runAction(MoveTo::create(0.7, Vec2(mm->getPositionX(), -150)));
				mm->runAction(Sequence::create(DelayTime::create(
					0.7), CallFunc::create([=]{mm->setVisible(false); })
					, nullptr));

			}

		}), DelayTime::create(2.0),
			CallFunc::create([=]{
			showLose(); })
				, nullptr
				));
			/////////////////////////////




	}


}

void  scene_3::call_11(float p)
{
	if (mm->getPositionX() - tank->getPositionX() < 140)
	{
		walk->setPositionX(-3000);

		mm1->setPosition(Vec2(tank->getPositionX() + 140, tank->getPositionY() - 20));
		tag1 = true;
		mm1->setVisible(true);
		walk->setVisible(false);
		mm->setVisible(false);

		mm1->runAction(RotateBy::create(1.0, 70));

		menu_tank_sp->setEnabled(false);
		this->runAction(
			Sequence::create(DelayTime::create(1.0),
			CallFunc::create([=]{
			mm1->setVisible(false); })
				, nullptr
				));

			unscheduleAllCallbacks();
			this->scheduleUpdate();
	}


}

void scene_3::call_man_1(Ref*p)
{
	log("man");
	menu_man_sp->setVisible(false);


	auto ani1 = Animation::create();
	man->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-jump-03.png"));

	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-jump-01.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-jump-02.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-jump-03.png"));
	ani1->setDelayPerUnit(2.8 / 30);
	ani1->setLoops(1);
	ani1->setRestoreOriginalFrame(true);
	man->runAction(Animate::create(ani1));

	man->runAction(JumpBy::create(1.0, Vec2(0, 0), 200, 1));
	man->runAction(MoveBy::create(1.0, Vec2(300, 0)));
	man->setPositionY(640 / 2 - 20);
	SimpleAudioEngine::getInstance()->playEffect("bridge_jump.mp3");



	this->runAction(Sequence::create(
		DelayTime::create(1.0),
		CallFunc::create([=]{
		//man->setVisible(false);
		man->setVisible(false);

		walk = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hero-jump-03.png"),
			Sprite::createWithSpriteFrameName("hero-jump-03.png"), nullptr, this, menu_selector(scene_3::call_walk));
		walk->setPosition(man->getPosition());

		auto me = Menu::create(walk, nullptr);
		me->setPosition(Point::ZERO);
		this->addChild(me);

		mm = Sprite::createWithSpriteFrameName("hero-walk-02.png");
		mm->setPosition(Vec2(5000, -200));
		this->addChild(mm);
		tagg = true;
		tag = true;


		call_walk(0);


	})

		, nullptr));


	//if (tagg == true)
	{

		this->runAction(
			Sequence::create(DelayTime::create(2.0), CallFunc::create([=]{

			schedule(schedule_selector(scene_3::call_11), 1 / 59.0); }
		)
			, nullptr));
	}

}

void scene_3::call_tank(Ref*p)
{
	log("tank");
	menu_tank_sp->setVisible(false);
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playBackgroundMusic("ji-jing.mp3",true);

}


void scene_3::call_walk(Ref*p)
{


	if (walk->getPositionX() > 1136 / 2 + 300)
	{
		mm->setVisible(true);
		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-walk-01.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-walk-02.png"));
		ani1->setDelayPerUnit(2.8 / 30);
		ani1->setLoops(1);
		ani1->setRestoreOriginalFrame(true);

		walk->setPositionX(3000);
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();
		SimpleAudioEngine::getInstance()->playEffect("bridge_hohoho.mp3");


		//	mm->setPosition(walk->getPosition());
		mm->runAction(RepeatForever::create(Animate::create(ani1)));
		mm->runAction(MoveBy::create(3.0, Vec2(420, 0)));
		this->runAction(Sequence::create(
			DelayTime::create(3.0),
			CallFunc::create([=]{showWin(); }), nullptr));

	}
	else
	{

		walk->setNormalImage(Sprite::createWithSpriteFrameName("hero-walk-02.png"));
		walk->setSelectedImage(Sprite::createWithSpriteFrameName("hero-walk-02.png"));


		mm->setVisible(true);
		walk->setVisible(false);

		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-walk-01.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-walk-02.png"));
		ani1->setDelayPerUnit(2.8 / 30);
		ani1->setLoops(3);
		ani1->setRestoreOriginalFrame(true);
		mm->setPosition(walk->getPosition());
		mm->runAction(Animate::create(ani1));
		walk->runAction(MoveBy::create(2.8 / 30 * 2 * 3, Vec2(20, 0)));
		mm->runAction(MoveBy::create(2.8 / 30 * 2 * 3, Vec2(20, 0)));
	
		this->runAction(Sequence::create(DelayTime::create(2.8 / 30 * 2 * 3),
			CallFunc::create([=]{
			walk->setVisible(true);
			mm->setVisible(false);
			SimpleAudioEngine::getInstance()->playEffect("bridge_stop.mp3");

			

			man->setVisible(false);
		}), nullptr

			));

	}


}





Scene*scene_4::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_4::create();
	scene->addChild(layer);
	return scene;

}


bool scene_4::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();


	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("clickme.plist");

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);



	menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("click_me_01.png"),
		Sprite::createWithSpriteFrameName("click_me_01.png"),
		nullptr
		, this, menu_selector(scene_4::call_lose));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button.png"),
		Sprite::createWithSpriteFrameName("button.png"),
		nullptr
		, this, menu_selector(scene_4::showWin));

	menu_11->setPosition(Vec2(1136 / 2 + 400, 640 / 2 + 200));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



	SimpleAudioEngine::getInstance()->playBackgroundMusic("ji-jing.mp3", true);

	this->addChild(publicMenu::createNew());
	return true;
}


void scene_4::showWin(Ref*p)
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");


	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));


	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0008.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);

	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_4::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_4::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_4::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_4::showLose()
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0007.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_4::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_4::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}



void scene_4::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");
	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_4::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");
	Director::getInstance()->replaceScene(scene_4::createScene());
}

void scene_4::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");
	Director::getInstance()->replaceScene(scene_5::createScene());

}

void scene_4::call_lose(Ref*p)
{

	this->runAction(Sequence::create(CallFunc::create(
		[=]{
		ss = Sprite::createWithSpriteFrameName("blow_up_01.png");

		ss->setPosition(Vec2(1136 / 2, 640 / 2));
		this->addChild(ss);

		ss->runAction(Blink::create(1.0, 10));
		SimpleAudioEngine::getInstance()->playEffect("boom.mp3");
		menu_11->setVisible(false);
		menu_1->setVisible(false);

	}

	), DelayTime::create(1.0),
		CallFunc::create([=]{
		showLose(); })
			, nullptr));
}





Scene*scene_5::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_5::create();
	scene->addChild(layer);
	return scene;

}


bool scene_5::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("play_the_plan.plist");

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);

	memset(bullet, 0, sizeof(bullet));
	memset(enemy, 0, sizeof(enemy));




	hero = Sprite::createWithSpriteFrameName("hero_fly_1.png");
	hero->setPosition(Vec2(1136 / 2, 640 / 2 - 200));
	this->addChild(hero);

	auto ani = Animation::create();
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_fly_1.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_fly_2.png"));
	ani->setDelayPerUnit(2.8 / 30);
	ani->setRestoreOriginalFrame(true);
	auto act = RepeatForever::create(Animate::create(ani));
	act->setTag(10);//
	hero->runAction(act);



	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(false);

	listener1->onTouchBegan = [=](Touch* touch, Event* event){
		auto target = static_cast<Sprite*>(event->getCurrentTarget());

		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);

		if (rect.containsPoint(locationInNode))
		{
			begain.x = locationInNode.x;//float
			begain.y = locationInNode.y;

		}

		return true;
	};


	listener1->onTouchMoved = [=](Touch* touch, Event* event){
		auto target = static_cast<Sprite*>(event->getCurrentTarget());

		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);

		if (rect.containsPoint(locationInNode))
		{


			if ((hero->getPositionY() + locationInNode.y - begain.y<640 - 40)
				&& (hero->getPositionY() + locationInNode.y - begain.y > 40))
			{

				hero->setPositionY(hero->getPositionY() + (locationInNode.y - begain.y));

				if (hero->getPositionX() < -40 || hero->getPositionX() >1136 + 40)
				{
					_eventDispatcher->removeAllEventListeners();
					showWin();
				}

			}

			hero->setPositionX(hero->getPositionX() + (locationInNode.x - begain.x));
			begain = locationInNode;

		}

	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, this);





	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("enemy1_blowup_1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("enemy1_blowup_2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("enemy1_blowup_3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("enemy1_blowup_4.png"));

	ani1->setDelayPerUnit(3.0 / 30 / 2.0);
	ani1->setRestoreOriginalFrame(true);

	AnimationCache::getInstance()->addAnimation(ani1, "ee");


	this->scheduleUpdate();
	this->addChild(publicMenu::createNew());


	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("play_the_plane_game_music.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("play_the_plane_game_music.mp3", true);



	return true;
}






void scene_5::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	this->unscheduleUpdate();
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_5::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);

	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0010.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_5::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_5::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_5::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_5::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_5::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0009.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_5::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_5::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


}



void scene_5::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_5::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_5::createScene());
}

void scene_5::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_6::createScene());

}



void scene_5::update(float a)
{

	//产生子弹
	i++;
	if (i > 10)
	{
		i = 0;
		for (int a = 0; a < 20; a++)
		{
			if (bullet[a] == nullptr)
			{
				bullet[a] = Sprite::createWithSpriteFrameName("enemy1_fly_1.png");
				bullet[a]->setPosition(Vec2(hero->getPositionX(), hero->getPositionY() + 50));
				bullet[a]->setFlipY(true);
				this->addChild(bullet[a]);
				bullet[a]->setScale(0.5);
				SimpleAudioEngine::getInstance()->playEffect("play_the_plane_bullet.mp3");


				break;


			}
		}


	}


	//移动子弹
	for (int a = 0; a < 20; a++)
	{
		if (bullet[a] != nullptr)
		{
			bullet[a]->setPositionY(bullet[a]->getPositionY() + 50);
			if (bullet[a]->getPositionY()>650)
			{
				this->removeChild(bullet[a], true);
				bullet[a] = nullptr;
			}
		}
	}



	//产生敌人
	ii++;
	if (ii > dua)
	{
		if (dua > 8.0)
			dua -= 5,
			log("%f", dua);

		ii = 0;
		for (int a = 0; a < 200; a++)
		{
			if (enemy[a] == nullptr)
			{
				enemy[a] = Sprite::createWithSpriteFrameName("enemy1_fly_1.png");
				enemy[a]->setPosition(Vec2(rand() % 1100 + 17, 650));
				this->addChild(enemy[a]);
				enemy[a]->setTag(rand() % 2 + 1);
				break;
			}
		}
	}

	//移动敌人
	for (int a = 0; a < 200; a++)
	{
		if (enemy[a] != nullptr)
		{
			enemy[a]->setPositionY(enemy[a]->getPositionY() - enemy[a]->getTag());
			if (enemy[a]->getPositionY() < -50)
			{
				this->removeChild(enemy[a], true);
				enemy[a] = nullptr;
			}
		}
	}

	//子弹碰撞检测
	for (int i = 0; i < 20; i++)
	{
		if (bullet[i] != nullptr)
		{
			for (int ii = 0; ii < 200; ii++)
			{
				if (enemy[ii] != nullptr)
				{
					x = abs(enemy[ii]->getPositionX() - bullet[i]->getPositionX());
					y = abs(enemy[ii]->getPositionY() - bullet[i]->getPositionY());

					if (x < 28 / 2 && y < 32 / 2)
					{
						enemy[ii]->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(
							"enemy1_blowup_4.png"));

						enemy[ii]->runAction(
							Sequence::create(
							Animate::create(
							AnimationCache::getInstance()->getAnimation("ee")),
							DelayTime::create(0.20),
							CallFunc::create([=]{
							enemy[ii]->setOpacity(0);

							SimpleAudioEngine::getInstance()->playEffect("play_the_plane_enemy1_down.mp3");
							
							this->removeChild(enemy[ii], true);
							enemy[ii] = nullptr;
						})

							, nullptr)

							);
					}

				}
			}


		}
	}



	//hero enemy碰撞检测

	for (int ii = 0; ii < 200; ii++)
	{
		if (enemy[ii] != nullptr)
		{
			x = abs(enemy[ii]->getPositionX() - hero->getPositionX());
			y = abs(enemy[ii]->getPositionY() - hero->getPositionY());

			if (x < 50 / 2 && y < 80 / 2)
			{
				this->unscheduleUpdate();
				_eventDispatcher->removeAllEventListeners();
				auto ani1 = Animation::create();
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_blowup_1.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_blowup_2.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_blowup_3.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_blowup_4.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_blowup_5.png"));
				ani1->setDelayPerUnit(3.0 / 30);
				ani1->setRestoreOriginalFrame(true);
				hero->runAction(Animate::create(ani1));

				SimpleAudioEngine::getInstance()->playEffect("play_the_plane_game_over.mp3");
				SimpleAudioEngine::getInstance()->stopBackgroundMusic();


				hero->runAction(Sequence::create(DelayTime::create(0.5),
					Blink::create(3.0, 6),
					CallFunc::create([=]{
					showLose();

				})

					, nullptr));
				//this->unscheduleUpdate();

			}

		}

	}

}







Scene*scene_6::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_6::create();
	scene->addChild(layer);
	return scene;

}


bool scene_6::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("cut_rope.plist");

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	rope = Sprite::createWithSpriteFrameName("rope.png");

	this->addChild(rope);
	rope->setColor(Color3B(85, 120, 40));

	rope->setTextureRect(Rect(2, 1344, length, 4));
	rope->setRotation(90);




	menu_ = MenuItemSprite::create(Sprite::createWithSpriteFrameName("botton.png"),
		Sprite::createWithSpriteFrameName("botton.png"), nullptr, this, menu_selector(scene_6::call_button)
		);
	menu_->setPosition(Vec2(1136 / 2, 640 / 2 + 160));

	auto menu = Menu::create(menu_, nullptr);
	this->addChild(menu);
	menu->setPosition(Point::ZERO);



	auto lin = Sprite::createWithSpriteFrameName("line.png");
	lin->setPosition(Vec2(1136 / 2, 640 / 2 + 120));
	this->addChild(lin);




	man = Sprite::createWithSpriteFrameName("hero-01.png");
	this->addChild(man);
	man->setPosition(Vec2(1136 / 2, 640 / 2 - 200));




	rope->setAnchorPoint(Vec2(0, 0.5));

	apple = Sprite::createWithSpriteFrameName("apple.png");
	apple->setAnchorPoint(Vec2(0.5, 1));
	this->addChild(apple);



	rope->setPosition(Vec2(1136 / 2, menu_->getPositionY()));
	apple->setPosition(Vec2(1136 / 2, menu_->getPositionY() - length + 50));

	hand = Sprite::createWithSpriteFrameName("hand.png");
	hand->setPosition(Vec2(1136 / 2 - 150, menu_->getPositionY() - 70));
	this->addChild(hand);

	hand->runAction(

		RepeatForever::create(Sequence::create(
		MoveBy::create(2.5, Vec2(350, 0)), MoveBy::create(0, Vec2(-350, 0))
		, nullptr)
		)
		);

	///***** 手指滑动/




	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = (Sprite*)(event->getCurrentTarget());
		auto point = target->convertToNodeSpace((touch->getLocation()));


		if (point.x > 338 && point.x < 520)
		if (point.y>(465 - length - 20) && point.y < 465)
		{


			return true;
		}

		return false;
	};

	listener1->onTouchMoved = [=](Touch*touch, Event*event){
		auto target = (Sprite*)(event->getCurrentTarget());
		auto point = target->convertToNodeSpace((touch->getLocation()));

		if (point.x > 600 && point.x<780)
		if (point.y>(465 - length - 20) && point.y < 460)
		{
			log("yes");


			_eventDispatcher->removeAllEventListeners();
			hand->setVisible(false);
			man->setTag(11);
			apple->runAction(MoveTo::create(1.0f, Vec2(1136 / 2, 640 / 2 - 20)));

			SimpleAudioEngine::getInstance()->playEffect("cut_rope_ping_guo.mp3");
			SimpleAudioEngine::getInstance()->playEffect("cut_rope_qie.mp3");

		}



	};


	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, this);


	this->addChild(publicMenu::createNew());
	this->scheduleUpdate();


	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("huaji-kongbu.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("huaji-kongbu.mp3", true);



	return true;
}



void scene_6::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_6::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0011.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_6::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_6::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_6::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_6::showLose()
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_6::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0012.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_6::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_6::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}






void scene_6::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());


}

void scene_6::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_6::createScene());
}

void scene_6::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_7::createScene());

}

void scene_6::update(float delta)
{
	if (apple->isVisible() == true)
	if (apple->getPositionY() < 640 / 2)
	{

		if (man->getTag() != 11 && apple->getTag() == 11)//没有割断  张嘴
		{
			//win
			this->unscheduleUpdate();
			_eventDispatcher->removeAllEventListeners();
			SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			SimpleAudioEngine::getInstance()->playEffect("cut_rope_chi.mp3");

			apple->setVisible(false);
			auto ani = Animation::create();
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-01.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-04.png"));
			ani->setDelayPerUnit(2 / 10.0);
			ani->setRestoreOriginalFrame(true);
			ani->setLoops(4);
			man->runAction(RepeatForever::create(Animate::create(ani)));
			man->runAction(Sequence::create(
			
				DelayTime::create(4.0),
				CallFunc::create([=]{
				showWin(); })

					, nullptr));

		}
		else
		{
			//die
			this->unscheduleUpdate();

			apple->setVisible(false);
			SimpleAudioEngine::getInstance()->playEffect("cut_rope_peng_tou.mp3");

			auto ani = Animation::create();
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-star-01.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-star-02.png"));
			ani->setDelayPerUnit(2 / 10.0);
			ani->setRestoreOriginalFrame(true);
			ani->setLoops(4);
			//man->runAction(RepeatForever::create(Animate::create(ani)));
			man->runAction(Sequence::create(
				Animate::create(ani),
				//DelayTime::create(3.0),
				CallFunc::create([=]{
				showLose(); })

					, nullptr));

		}
	}

}


void scene_6::call_button(Ref*p)
{
	length += 20;
	rope->setTextureRect(Rect(2, 1344, length, 4));
	apple->runAction(MoveBy::create(1.0, Vec2(0, -20)));
	rope->setPosition(Vec2(1136 / 2, menu_->getPositionY()));
	SimpleAudioEngine::getInstance()->playEffect("show.mp3");



	if (apple->getPositionY() < 640 / 2 + 50)
	{
		apple->setTag(11);//张开了嘴
		man->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-02.png"));
	}

}






Scene*scene_7::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_7::create();
	scene->addChild(layer);
	return scene;

}


bool scene_7::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();


	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("drive.plist");

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	car = MenuItemSprite::create(
		Sprite::createWithSpriteFrameName("car.png"), Sprite::createWithSpriteFrameName("car.png"), nullptr
		, this, menu_selector(scene_7::call_car));
	car->setPosition(Vec2(0, 640 / 2 - 150));
	auto ss = Menu::create(car, nullptr);
	ss->setPosition(Point::ZERO);
	this->addChild(ss);


	boom = Sprite::createWithSpriteFrameName("boom.png");
	boom->setPosition(Vec2(1136 / 2, 640 / 2 - 200));
	this->addChild(boom);

	wood = Sprite::createWithSpriteFrameName("mine.png");
	wood->setPosition(boom->getPositionX() + 400, boom->getPositionY() - 20);
	this->addChild(wood);



	auto ani = Animation::create();
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_01.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_02.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_03.png"));



	ani->setDelayPerUnit(3.0 / 30);
	ani->setLoops(3);
	ani->setRestoreOriginalFrame(true);

	AnimationCache::getInstance()->addAnimation(ani, "boom");



	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		beg = target->convertToNodeSpace(touch->getLocation());

		////木头 227*142
		if (abs(beg.x - wood->getPositionX()) < 220 / 2 &&
			abs(beg.y - wood->getPositionY()) < 140 / 2)
		{


			return true;
		}

		return false;
	};


	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		Point beg2 = target->convertToNodeSpace(touch->getLocation());

		wood->setPositionX(beg2.x);

		if (beg2.y < 640 / 2 - 100 && beg2.y>20)
		{
			wood->setPositionY(beg2.y);
			return;
		}


	};


	listener->onTouchEnded = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto beg1 = target->convertToNodeSpace(touch->getLocation());

		if (beg1.x == beg.x&&beg.y == beg1.y)
		{

			_eventDispatcher->removeAllEventListeners();
			this->unscheduleUpdate();

			//双击木头结束


			wood->runAction(Sequence::create(
				Animate::create(AnimationCache::getInstance()->getAnimation("boom")),
				//	DelayTime::create(0.3),
				CallFunc::create([=]{
				showLose();
			})
				, nullptr
				));


		}


	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());
	this->scheduleUpdate();


	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("bao-zhou.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("bao-zhou.mp3", true);



	return true;
}

////


void scene_7::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_7::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0014.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_7::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_7::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_7::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_7::showLose()
{
	

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_7::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0013.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_7::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_7::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}




void scene_7::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_7::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_7::createScene());
}

void scene_7::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_8::createScene());

}





void scene_7::update(float delta)
{
	//car wood碰撞检测
	if (
		//wood->getPositionY() < boom->getPositionY()+20) // &&

		(wood->getPositionY() < 640 / 2 - 150 && wood->getPositionY()>20)

		&&
		(abs(wood->getPositionX() - car->getPositionX()) < 200)
		)

	{
		this->unscheduleUpdate();
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();
		SimpleAudioEngine::getInstance()->playEffect("boom.mp3");

		_eventDispatcher->removeAllEventListeners();

		wood->runAction(Sequence::create(
			Animate::create(AnimationCache::getInstance()->getAnimation("boom")),
			//	DelayTime::create(0.3),
			CallFunc::create([=]{
			wood->setVisible(false);
			showLose();
		})
			, nullptr
			));

	}



	if (run == true)
	{
		//SimpleAudioEngine::getInstance()->playEffect("drive_car.mp3",true);


		car->setPositionX(car->getPositionX() + 5);
		if (car->getPositionX() > 1000)
		{
			this->unscheduleUpdate();
			_eventDispatcher->removeAllEventListeners();
			SimpleAudioEngine::getInstance()->stopAllEffects();
			showWin();
		}
	}

}


void scene_7::call_car(Ref*p)
{
	run = !run;

	if (run == false)
	{
		SimpleAudioEngine::getInstance()->stopAllEffects();
	
	}
	else
	{


		SimpleAudioEngine::getInstance()->playEffect("drive_car.mp3",true);

	}



	if (isBoom == false)
	{
		//设置定时器 引爆
		isBoom = true;
		//播放炸弹动画


		boom_ = Sprite::createWithSpriteFrameName("line-of-fire.png");
		boom_->setPosition(boom->getPositionX() + 30, boom->getPositionY() + 50);
		this->addChild(boom_);

		boom_->runAction(RepeatForever::create(Sequence::create(
			RotateBy::create(0.2, 30), RotateBy::create(0.2, -30),
			nullptr
			)));

		//炸弹结束游戏
		scheduleOnce([=](float p){

			SimpleAudioEngine::getInstance()->playEffect("boom.mp3");

			if (boom->getPositionX() - car->getPositionX() < 220)
			{
				//失败
				boom_->setVisible(false);
				_eventDispatcher->removeAllEventListeners();
				this->unscheduleUpdate();
	
				boom->runAction(Sequence::create(
					Animate::create(AnimationCache::getInstance()->getAnimation("boom")),
					//	DelayTime::create(0.3),
					CallFunc::create([=]{
					boom->setVisible(false);
					showLose();
				})
					, nullptr
					));
			}
			else
			{
				boom_->setVisible(false);
				boom->runAction(Sequence::create(
					Animate::create(AnimationCache::getInstance()->getAnimation("boom")),
					//	DelayTime::create(0.3),
					CallFunc::create([=]{
					boom->setVisible(false);
				})
					, nullptr
					));
			}

		}, 1.5, "boom");

	}

}






Scene*scene_8::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_8::create();
	scene->addChild(layer);
	return scene;

}



bool scene_8::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("newCliff.plist");

	auto bg = Sprite::createWithSpriteFrameName("Cliffbg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);



	man = Sprite::createWithSpriteFrameName("walk_01.png");
	man->setPosition(-50, 640 / 2 - 20);
	this->addChild(man);

	auto ani = Animation::create();
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("walk_01.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("walk_02.png"));

	ani->setDelayPerUnit(4.0 / 30.0);
	ani->setRestoreOriginalFrame(false);
	ani->setLoops(19);
	man->runAction(Animate::create(ani));


	man->runAction(Sequence::create(
		MoveTo::create(5.0, Vec2(1136 / 2 + 170, 640 / 2 - 20)),

		CallFunc::create([=]{
		if (wu->getOpacity() >= 1)
		{//lose
			SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			SimpleAudioEngine::getInstance()->playEffect("cliff_falldownscreem.mp3");
			auto ani = Animation::create();
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fall_01.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fall_02.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fall_03.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fall_04.png"));
			ani->setDelayPerUnit(3.0 / 30.0);
			ani->setRestoreOriginalFrame(true);
			ani->setLoops(1);

			man->runAction(Sequence::create(
				Animate::create(ani),DelayTime::create(1.0),
				CallFunc::create([=]{
				man->setVisible(false);
				showLose();
			})

				, nullptr));

			man->runAction(MoveBy::create(3.0 / 30.0 * 4, Vec2(30, -(640 / 2 + 100))));

		}

		if (wu->getOpacity() == 0)
		{
			auto ani = Animation::create();

			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("rise01.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bow01.png"));

			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("rise02.png"));

			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bow02.png"));

			ani->setDelayPerUnit(4.0 / 30.0);
			ani->setRestoreOriginalFrame(false);
			ani->setLoops(1);
			man->runAction(Animate::create(ani));

		}
	})
		, nullptr));



	lab = Sprite::createWithSpriteFrameName("no danger.png");
	lab->setPosition(1136 / 2 + 200, 640 / 2 + 200);
	this->addChild(lab);

	lab->setScale(0.8);


	wu = MenuItemSprite::create(Sprite::createWithSpriteFrameName("no.png"),
		Sprite::createWithSpriteFrameName("no.png"), nullptr, this, menu_selector(scene_8::call_wu));

	wu->setPosition(1136 / 2 + 200, 640 / 2 + 200);

	wu->setScale(0.8);

	auto sss = Menu::create(wu, nullptr);
	sss->setPosition(0, 0);
	this->addChild(sss);


	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());


		if (wu->getOpacity() == 0)//擦掉 "无"
		{
			if ((abs(point.x - 740) < 60) &&
				(abs(point.y - 300) < 90))
			{
				_eventDispatcher->removeAllEventListeners();
				///////////

				SimpleAudioEngine::getInstance()->playEffect("clickMe_haha.mp3");
				SimpleAudioEngine::getInstance()->playEffect("cliff_backwalklaugh.mp3");
				auto ani = Animation::create();
				ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("walk_01.png"));
				ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("walk_02.png"));
				ani->setDelayPerUnit(3.0 / 30.0);
				ani->setRestoreOriginalFrame(false);
				ani->setLoops(19);
				man->runAction(Animate::create(ani));

				man->setFlippedX(true);
				man->runAction(Sequence::create(
					MoveTo::create(3.0, Vec2(-50, 640 / 2 - 20)),
					CallFunc::create([=]{
					showWin(); })
						, nullptr));
			}
			else
			{
				auto ani = Animation::create();
				ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("rise01.png"));
				ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bow01.png"));
				ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("rise02.png"));
				ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bow02.png"));
				ani->setDelayPerUnit(4.0 / 30.0);
				ani->setRestoreOriginalFrame(false);
				ani->setLoops(1);
				man->runAction(Animate::create(ani));


				lab->runAction(Sequence::create(
					ScaleTo::create(0.8, 0.7),
					ScaleTo::create(0.8, 1)
					, nullptr
					));
			}

		}
		return false;
	};


	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());
	this->scheduleUpdate();

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("huaji-kongbu.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("huaji-kongbu.mp3", true);



	return true;

}



void scene_8::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_8::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0016.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_8::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_8::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_8::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_8::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");



	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_8::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0015.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_8::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_8::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}




void scene_8::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_8::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_8::createScene());
}

void scene_8::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_9::createScene());

}



void scene_8::call_wu(Ref*p)
{
	if (wu->getOpacity() < 30)
	{
		_eventDispatcher->setEnabled(false);
		wu->setOpacity(0);
		wu->setEnabled(false);
		lab->runAction(ScaleTo::create(2.0, 1.0));
		this->runAction(Sequence::create(DelayTime::create(2.0),
			CallFunc::create([=]{_eventDispatcher->setEnabled(true); })
			, nullptr));
	}
	else
	{
		wu->setOpacity(wu->getOpacity() - 30);
	}

}





Scene*scene_9::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_9::create();
	scene->addChild(layer);
	return scene;

}



bool scene_9::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("catch_mouse.plist");

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);

	/*
	洞
	*/
	/*
	auto h1 = Sprite::createWithSpriteFrameName("hole.png");
	h1->setPosition(1136 / 2, 640 / 2-50);
	this->addChild(h1);

	auto h2 = Sprite::createWithSpriteFrameName("hole.png");
	h2->setPosition(1136 / 2, 640 / 2-250);
	this->addChild(h2);

	auto h3 = Sprite::createWithSpriteFrameName("hole.png");
	h3->setPosition(1136 / 2-300, 640 / 2 - 50);
	this->addChild(h3);

	auto h4 = Sprite::createWithSpriteFrameName("hole.png");
	h4->setPosition(1136 / 2-300, 640 / 2 - 250);
	this->addChild(h4);

	auto h5 = Sprite::createWithSpriteFrameName("hole.png");
	h5->setPosition(1136 / 2 + 300, 640 / 2 - 50);
	this->addChild(h5);

	auto h6 = Sprite::createWithSpriteFrameName("hole.png");
	h6->setPosition(1136 / 2 + 300, 640 / 2 - 250);
	this->addChild(h6);

	*/


	hole[0] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hole.png"),
		Sprite::createWithSpriteFrameName("hole.png"), nullptr, this, menu_selector(scene_9::call_hole_1));
	hole[0]->setPosition(1136 / 2 - 320, 640 / 2 - 50);


	hole[1] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hole.png"),
		Sprite::createWithSpriteFrameName("hole.png"), nullptr, this, menu_selector(scene_9::call_hole_2));
	hole[1]->setPosition(1136 / 2, 640 / 2 - 50);


	hole[2] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hole.png"),
		Sprite::createWithSpriteFrameName("hole.png"), nullptr, this, menu_selector(scene_9::call_hole_3));
	hole[2]->setPosition(1136 / 2 + 320, 640 / 2 - 50);


	hole[3] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hole.png"),
		Sprite::createWithSpriteFrameName("hole.png"), nullptr, this, menu_selector(scene_9::call_hole_4));
	hole[3]->setPosition(1136 / 2 - 320, 640 / 2 - 250);


	hole[4] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hole.png"),
		Sprite::createWithSpriteFrameName("hole.png"), nullptr, this, menu_selector(scene_9::call_hole_5));
	hole[4]->setPosition(1136 / 2, 640 / 2 - 250);


	hole[5] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hole.png"),
		Sprite::createWithSpriteFrameName("hole.png"), nullptr, this, menu_selector(scene_9::call_hole_6));
	hole[5]->setPosition(1136 / 2 + 320, 640 / 2 - 250);


	auto me_ = MenuItemSprite::create(Sprite::createWithSpriteFrameName("tap.png"),
		Sprite::createWithSpriteFrameName("tap.png"), nullptr, this, menu_selector(scene_9::call_tap));
	me_->setPosition(1136 / 2 + 320, 640 / 2 + 70);



	stone = MenuItemSprite::create(Sprite::createWithSpriteFrameName("rock.png"),
		Sprite::createWithSpriteFrameName("rock.png"), nullptr, this, menu_selector(scene_9::call_rock));
	stone->setPosition(1136 / 2 - 55, 640 / 2 + 82);


	man = MenuItemSprite::create(Sprite::createWithSpriteFrameName("people.png"),
		Sprite::createWithSpriteFrameName("people.png"), nullptr, this, menu_selector(scene_9::aaaa));

	auto menu_ = Menu::create(me_, stone, hole[0], hole[1], hole[2],
		hole[3], hole[4], hole[5], man, nullptr);
	menu_->setPosition(0, 0);
	this->addChild(menu_);


	for (int i = 0; i < 6; i++)
	{
		bigRock[i] = Sprite::createWithSpriteFrameName("bigrock.png");
		bigRock[i]->setPositionX(hole[i]->getPositionX());
		bigRock[i]->setPositionY(hole[i]->getPositionY() + 40);
		bigRock[i]->setVisible(false);
		this->addChild(bigRock[i]);

	}

	manPos = rand() % 6;

	randManPos(0);


	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());


		return false;
	};


	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());
	this->scheduleUpdate();


	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("catch_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("catch_BG.mp3", true);



	return true;

}



void scene_9::call_rock(Ref*p)//点击 小石头回调
{
	isRock = true;

	SimpleAudioEngine::getInstance()->playEffect("show.mp3");

}


void scene_9::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_9::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));


	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0018.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_9::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_9::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_9::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_9::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_7::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0017.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_9::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_9::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}




void scene_9::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_9::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_9::createScene());
}

void scene_9::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_10::createScene());

}




void scene_9::call_hole_1(Ref*p)
{


	if (bigRock[0]->isVisible() == false)
	{

		log("%d %d", no, isRock);
		if (no == 5)
		{
			showWin();
		}
		else
		{
			if (manPos == 0)randManPos(0);
		}


		if (isRock == true)
		{
			no++;
			bigRock[0]->setVisible(true);
			SimpleAudioEngine::getInstance()->playEffect("catch_mouse_rock.mp3");
			isRock = false;
		}


	}


}

void scene_9::call_hole_2(Ref*p)
{


	if (bigRock[1]->isVisible() == false)
	{

		log("%d %d", no, isRock);
		if (no == 5)
		{
			showWin();
		}
		else
		{
			if (manPos == 1)randManPos(0);
		}


		if (isRock == true)
		{
			no++;
			SimpleAudioEngine::getInstance()->playEffect("catch_mouse_rock.mp3");
			bigRock[1]->setVisible(true);
			isRock = false;
		}

	}

}


void scene_9::call_hole_3(Ref*p)
{

	if (bigRock[2]->isVisible() == false)
	{

		log("%d %d", no, isRock);
		if (no == 5)
		{
			showWin();
		}
		else
		{
			if (manPos == 2)randManPos(0);
		}


		if (isRock == true)
		{
			no++;
			bigRock[2]->setVisible(true);
			SimpleAudioEngine::getInstance()->playEffect("catch_mouse_rock.mp3");
			isRock = false;
		}


	}



}

void scene_9::call_hole_4(Ref*p)
{


	if (bigRock[3]->isVisible() == false)
	{

		log("%d %d", no, isRock);
		if (no == 5)
		{
			showWin();
		}
		else
		{
			if (manPos == 3)randManPos(0);
		}


		if (isRock == true)
		{
			no++;
			bigRock[3]->setVisible(true);
			SimpleAudioEngine::getInstance()->playEffect("catch_mouse_rock.mp3");
			isRock = false;
		}


	}

}

void scene_9::call_hole_5(Ref*p)
{


	if (bigRock[4]->isVisible() == false)
	{

		log("%d %d", no, isRock);
		if (no == 5)
		{
			showWin();
		}
		else
		{
			if (manPos == 4)randManPos(0);
		}


		if (isRock == true)
		{
			no++;
			bigRock[4]->setVisible(true);
			SimpleAudioEngine::getInstance()->playEffect("catch_mouse_rock.mp3");
			isRock = false;
		}


	}
}

void scene_9::call_hole_6(Ref*p)
{

	if (bigRock[5]->isVisible() == false)
	{

		log("%d %d", no, isRock);
		if (no == 5)
		{
			showWin();
		}
		else
		{
			if (manPos == 5)randManPos(0);
		}


		if (isRock == true)
		{

			no++;
			bigRock[5]->setVisible(true);
			SimpleAudioEngine::getInstance()->playEffect("catch_mouse_rock.mp3");
			isRock = false;
		}


	}




}



void scene_9::aaaa(Ref*p)//
{
	if (bigRock[manPos]->isVisible() == false)
	{

		if (isRock == true)
		{
			no++;
			bigRock[manPos]->setVisible(true);
			isRock = false;
			SimpleAudioEngine::getInstance()->playEffect("catch_mouse_rock.mp3");
			if (no == 6)
			{
				showWin();
			}
			else
			{
				randManPos(0);
			}
		}

		else
		{

			if (no == 5)
			{
				showWin();
			}
			else
			{
				randManPos(0);
			}
		}

	}

}




void scene_9::randManPos(Ref*p)//随机产生合法位置 并且移动到该位置
{
	int tmp;
	SimpleAudioEngine::getInstance()->playEffect("show.mp3");

	while (1)
	{

		while (1)
		{
			tmp = rand() % 6;
			if (tmp != manPos)
			{
				goto b;
			}
		}
	b:{}
		if (bigRock[tmp]->isVisible() == false)//合法
		{
			manPos = tmp;
			switch (manPos)
			{
			case 0:
			{
					  man->setPosition(1136 / 2 - 320, 640 / 2 + 10);
					  goto a;
					  break;
			}
				break;


			case 1:
			{
					  man->setPosition(1136 / 2, 640 / 2 + 10);
					  goto a;
					  break;
			}
				break;

			case 2:
			{
					  man->setPosition(1136 / 2 + 320, 640 / 2 + 10);
					  goto a;
					  break;
			}
				break;

			case 3:
			{
					  man->setPosition(1136 / 2 - 320, 640 / 2 - 190);
					  goto a;
					  break;
			}
				break;

			case 4:
			{
					  man->setPosition(1136 / 2, 640 / 2 - 190);
					  goto a;
					  break;
			}
				break;

			case 5:
			{
					  man->setPosition(1136 / 2 + 320, 640 / 2 - 190);
					  goto a;
					  break;
			}
				break;


			}
		}

	}
a:{}


}




void scene_9::call_tap(Ref*p)
{

	_eventDispatcher->removeAllEventListeners();

	man->runAction(RotateTo::create(2.0, 90));
	man->setScale(0.7);
	man->setPositionY(man->getPositionY() - 20);

	for (int i = 0; i < 6; i++)
	{
		if (bigRock[i]->isVisible() == false)
		{
			Sprite*wa = Sprite::createWithSpriteFrameName("bigwater.png");
			wa->setPosition(bigRock[i]->getPositionX() + 2, bigRock[i]->getPositionY() - 14);
			this->addChild(wa);
			wa->setScale(0.1);
			wa->runAction(ScaleTo::create(0.8, 1));

		}

	}
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("catch_mouse_wather.mp3");
	SimpleAudioEngine::getInstance()->playEffect("chan-jiao.mp3");
	this->runAction(Sequence::create(
		DelayTime::create(2.3), CallFunc::create([=]{

		showLose(); }), nullptr
			));

}










Scene*scene_10::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_10::create();
	scene->addChild(layer);
	return scene;

}



bool scene_10::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("newfight_for_princess.plist");

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);

	auto title = Sprite::createWithSpriteFrameName("they are fighting.png");
	title->setPosition(1136 / 2, 640 / 2 + 220);
	this->addChild(title);
	title->setScale(0.6);

	man = MenuItemSprite::create(Sprite::createWithSpriteFrameName("hero.png"),
		Sprite::createWithSpriteFrameName("hero.png"), nullptr, this, menu_selector(scene_10::call_man));

	man->setPosition(1136 / 2 - 270, 640 / 2);

	bad = MenuItemSprite::create(Sprite::createWithSpriteFrameName("bad.png"),
		Sprite::createWithSpriteFrameName("bad.png"), nullptr, this, menu_selector(scene_10::call_bad));

	bad->setPosition(1136 / 2 + 270, 640 / 2);

	auto ap = Sprite::createWithSpriteFrameName("bad.png");
	ap->setTextureRect(Rect(102, 715, 243 - 102, 823 - 715));
	bad_head = MenuItemSprite::create(ap, ap, nullptr, this,
		menu_selector(scene_10::call_bad_head));
	bad_head->setPosition(1136 / 2 + 262, 640 / 2 + 99);

	auto ap1 = Sprite::createWithSpriteFrameName("hero.png");
	ap1->setTextureRect(Rect(1545, 1075, 1666 - 1550, 1210 - 1075));

	man_head = MenuItemSprite::create(ap1, ap1, nullptr, this,
		menu_selector(scene_10::call_man_head));
	man_head->setPosition(1136 / 2 - 278, 640 / 2 + 133);

	man_head->setRotation(-90);

	auto sss = Menu::create(man, man_head, bad, bad_head, nullptr);
	sss->setPosition(0, 0);
	this->addChild(sss);



	bad_head->setVisible(false);
	this->addChild(publicMenu::createNew());
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("fighter_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("fighter_BG.mp3", true);
	SimpleAudioEngine::getInstance()->playEffect("fight_dang.mp3");

	return true;

}



void scene_10::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_10::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0019.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_10::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_10::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_10::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_10::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_10::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0020.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_10::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_10::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}




void scene_10::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_10::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_10::createScene());
}

void scene_10::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_11::createScene());

}




void scene_10::call_man_head(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("fight_heartfly.mp3");
	man_head->setVisible(false);
	man->setEnabled(false);
	bad->setEnabled(false);
	isManHead = true;
	man->setNormalImage(Sprite::createWithSpriteFrameName("hero04.png"));
	man->setSelectedImage(Sprite::createWithSpriteFrameName("hero04.png"));

	auto love = Sprite::createWithSpriteFrameName("love03.png");
	love->setPosition(man_head->getPosition());
	this->addChild(love);
	love->runAction(MoveBy::create(2.0, Vec2(540, 0)));


	this->runAction(Sequence::create(
		DelayTime::create(2.0),
		CallFunc::create([=]{



		bad->setVisible(false);

		bad->setNormalImage(Sprite::createWithSpriteFrameName("bad08.png"));
		bad->setSelectedImage(Sprite::createWithSpriteFrameName("bad08.png"));

		auto bad1 = Sprite::createWithSpriteFrameName("bad08.png");
		bad1->setPosition(bad->getPosition());
		this->addChild(bad1);

		auto ani = Animation::create();
		ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad07.png"));
		ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad08.png"));

		ani->setDelayPerUnit(3.0 / 30);
		ani->setLoops(3);
		ani->setRestoreOriginalFrame(false);

		bad1->runAction(Sequence::create(
			Animate::create(ani),
			CallFunc::create([=]{

			man->setEnabled(true);
			bad->setEnabled(true);
			love->setVisible(false);

			bad->setVisible(true);
			bad1->setVisible(false);
			bad_head->setVisible(true); })

				, nullptr));

	})
		, nullptr));

}



void scene_10::call_man(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("fight_kingwalk.mp3");
	man_head->setVisible(false);
	man->setVisible(false);
	bad->setVisible(false);
	bad_head->setVisible(false);

	auto bad1 = Sprite::createWithSpriteFrameName("bad.png");
	bad1->setPosition(bad->getPosition());
	this->addChild(bad1);

	Sprite*man1;
	if (isManHead == false)
	{
		man1 = Sprite::createWithSpriteFrameName("hero.png");

	}
	else
	{
		man1 = Sprite::createWithSpriteFrameName("hero04.png");

	}
	man1->setPosition(man->getPosition());
	this->addChild(man1);

	bad1->runAction(MoveBy::create(1.0, Vec2(-350, 0)));

	auto ani = Animation::create();
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad01.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad03.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad05.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad06.png"));
	ani->setDelayPerUnit(3.0 / 30);
	ani->setLoops(3);
	ani->setRestoreOriginalFrame(true);
	bad1->runAction(Animate::create(ani));

	this->runAction(Sequence::create(DelayTime::create(1.1),
		CallFunc::create([=]{
		SimpleAudioEngine::getInstance()->playEffect("fight_bumb.mp3");

		auto ani = Animation::create();
		ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad.png"));
		ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad04.png"));
		ani->setDelayPerUnit(3.0 / 30);
		ani->setLoops(1);
		ani->setRestoreOriginalFrame(false);
		bad1->runAction(Animate::create(ani));

		//showLose();

	}), DelayTime::create(3.0 / 30.0 * 2),
		CallFunc::create([=]{


		auto ani = Animation::create();
		ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero2-1.png"));
		ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero2-2.png"));
		ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero2-3.png"));
		SimpleAudioEngine::getInstance()->playEffect("runafter_dogbitman.mp3");
		ani->setDelayPerUnit(3.0 / 30);
		ani->setLoops(1);
		ani->setRestoreOriginalFrame(false);
		man1->runAction(Animate::create(ani));
		man1->runAction(MoveBy::create(0.2, Vec2(0, -110)));
	})
		, DelayTime::create(0.5),
		CallFunc::create([=]{
		showLose();
	})
		, nullptr));

}

void scene_10::call_bad(Ref*p)
{
	call_man(0);
}



void scene_10::call_bad_head(Ref*p)
{
	man_head->setVisible(false);
	bad_head->setVisible(false);

	auto light = Sprite::createWithSpriteFrameName("light02.png");
	light->setPosition(1136 / 2 - 50, man_head->getPositionY() - 10);
	this->addChild(light);
	
	SimpleAudioEngine::getInstance()->playEffect("fight_electricity.mp3");

	auto ani = Animation::create();
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("light02.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("light01.png"));


	ani->setDelayPerUnit(2.0 / 30);
	ani->setLoops(3);
	ani->setRestoreOriginalFrame(false);
	light->runAction(Sequence::create(
		Animate::create(ani),
		CallFunc::create([=]{
		light->setVisible(false); }),
			CallFunc::create([=]{
			//man->setVisible(false);
			//bad->setVisible(false);

			auto love = Sprite::createWithSpriteFrameName("biglove.png");
			love->setPosition(1136 / 2, 640 / 2 - 80);
			this->addChild(love);
			love->setScale(0.45);;
			love->runAction(ScaleTo::create(0.7, 0.7));

			auto mann = Sprite::createWithSpriteFrameName("hero05.png");
			mann->setPosition(man->getPosition());
			this->addChild(mann);
			man->setVisible(false);
			auto ani = Animation::create();
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero05.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero06.png"));

			ani->setDelayPerUnit(3.0 / 30);
			ani->setLoops(1);
			ani->setRestoreOriginalFrame(false);
			mann->runAction(RepeatForever::create(Animate::create(ani)));

			SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			auto badd = Sprite::createWithSpriteFrameName("bad10.png");
			badd->setPosition(bad->getPosition());
			this->addChild(badd);
			bad->setVisible(false);
			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad09.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bad10.png"));

			ani1->setDelayPerUnit(3.0 / 30);
			ani1->setLoops(1);
			ani1->setRestoreOriginalFrame(false);
			badd->runAction(RepeatForever::create(Animate::create(ani1)));

			auto lab = Sprite::createWithSpriteFrameName("they are fall in love.png");
			lab->setPosition(1136 / 2, 640 / 2 - 220);
			this->addChild(lab);
			lab->setScale(0.7);
			lab->runAction((Sequence::create(
				ScaleTo::create(0.5, 1.3), nullptr)));
			SimpleAudioEngine::getInstance()->playEffect("fight_waou.mp3");

		}),
			DelayTime::create(2.0),
			CallFunc::create([=]{
			_eventDispatcher->removeAllEventListeners();

			showWin(); })
				, nullptr
				));

}





Scene*scene_11::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_11::create();
	scene->addChild(layer);
	return scene;

}


bool scene_11::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Rcting_Textrue.plist");

	auto bg = Sprite::createWithSpriteFrameName("bg2.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);

	memset(enemy, 0, sizeof(enemy));


	car = Sprite::createWithSpriteFrameName("car_0.png");
	car->setPosition(1136 / 2 - 430, 640 / 2 - 180);
	this->addChild(car);



	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(false);

	listener1->onTouchBegan = [=](Touch* touch, Event* event){
		auto target = (Sprite*)(event->getCurrentTarget());
		Point locationInNode = target->convertToNodeSpace(touch->getLocation());

		y = locationInNode.y;

		return true;
	};


	listener1->onTouchMoved = [=](...){
		tag = true;

	};


	listener1->onTouchEnded = [=](Touch* touch, Event* event){
		auto target = (Sprite*)(event->getCurrentTarget());

		Point locationInNode = target->convertToNodeSpace(touch->getLocation());

		if (tag == false)return;

		if (locationInNode.y - y > 0)
		{
			car->runAction(MoveTo::create(0.1, Vec2(1136 / 2 - 430, 640 / 2)));
			SimpleAudioEngine::getInstance()->playEffect("carRacing_changeline.mp3");
			tag = false;
			return;

		}

		if (locationInNode.y - y < 0)
		{
			car->runAction(MoveTo::create(0.1, Vec2(1136 / 2 - 430, 640 / 2 - 180)));
			SimpleAudioEngine::getInstance()->playEffect("carRacing_changeline.mp3");
			tag = false;
			return;
		}

		log("ended");
	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, this);

	this->addChild(publicMenu::createNew());
	this->scheduleUpdate();
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("carRacing_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("carRacing_BG.mp3", true);
	return true;
}






void scene_11::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_11::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);

	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0021.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_11::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_11::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_11::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_11::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_11::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0022.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_11::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_11::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


}



void scene_11::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_11::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_11::createScene());

}

void scene_11::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_12::createScene());

}



void scene_11::update(float a)
{

	i++;
	//产生敌人
	if (i > 110)
	{
		i = 0;
		for (int a = 0; a < 10; a++)
		{
			if (enemy[a] == nullptr)
			{
				tmp = rand() % 9 + 1;
				sprintf(str, "car_%d.png", tmp);

				enemy[a] = Sprite::createWithSpriteFrameName(str);
				if (rand() % 2 == 1)
				{

					enemy[a]->setPosition(Vec2(1136 + 180, 640 / 2));
				}
				else
					enemy[a]->setPosition(Vec2(1136 + 180, 640 / 2 - 180));
				this->addChild(enemy[a]);
				enemy[a]->setTag(tmp);
				break;
			}
		}
	}

	//移动敌人
	for (int a = 0; a < 10; a++)
	{
		if (enemy[a] != nullptr)
		{
			enemy[a]->setPositionX(enemy[a]->getPositionX() - 7);
			if (enemy[a]->getPositionX() < -120)
			{
				this->removeChild(enemy[a], true);
				enemy[a] = nullptr;
			}
		}
	}



	//car enemy碰撞检测

	for (int ii = 0; ii < 10; ii++)
	{
		if (enemy[ii] != nullptr)
		{
			if ((enemy[ii]->getPositionX() < 1136 / 2 - 280 && enemy[ii]->getPositionX()>20)
				&& enemy[ii]->getPositionY() == car->getPositionY())
			{

				if (enemy[ii]->getTag() != 9)
				{
					_eventDispatcher->removeAllEventListeners();
					this->unscheduleUpdate();
					auto ani1 = Animation::create();
					ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow up_01.png"));
					ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow up_02.png"));
					ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow up_03.png"));
					ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow up_04.png"));
					ani1->setLoops(3);
					ani1->setDelayPerUnit(3.0 / 30);
					ani1->setRestoreOriginalFrame(false);
					SimpleAudioEngine::getInstance()->stopBackgroundMusic();
					SimpleAudioEngine::getInstance()->playEffect("boom.mp3");
					enemy[ii]->runAction(Sequence::create(Animate::create(ani1),
						CallFunc::create([=]{
						showLose();
					})
						, nullptr));

				}
				else

				{
					SimpleAudioEngine::getInstance()->stopBackgroundMusic();
					SimpleAudioEngine::getInstance()->playEffect("carRacing_hehe.mp3");
					_eventDispatcher->removeAllEventListeners();
					this->unscheduleUpdate();
					car->runAction(Sequence::create(Blink::create(2.0, 3),
						CallFunc::create([=]{
						showWin();
					})
						, nullptr));


				}
			}

		}

	}

}









Scene*scene_12::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_12::create();
	scene->addChild(layer);
	return scene;

}


bool scene_12::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();


	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("sprite_afraid_sunshine.plist");


	bg = Sprite::createWithSpriteFrameName("afraid_sunshine_bg.png"),
		bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_hand_01.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_hand_02.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_hand_03.png"));
	ani1->setLoops(3);
	ani1->setDelayPerUnit(2.5 / 30);
	ani1->setRestoreOriginalFrame(false);

	man_sp = Sprite::createWithSpriteFrameName("man_hand_01.png");
	man_sp->runAction(RepeatForever::create(Spawn::create(Animate::create(ani1),
		JumpBy::create(0.5, Vec2(0, 1), 1, 2), nullptr)));

	man = MenuItemSprite::create(man_sp, man_sp, nullptr, this, menu_selector(scene_12::call_man));
	man->setPosition(1136 / 2 - 270, 640 / 2 - 235);

	ghost_sp = Sprite::createWithSpriteFrameName("ghost_01.png");

	ghost = MenuItemSprite::create(ghost_sp, ghost_sp, nullptr, this, menu_selector(scene_12::call_ghost));
	ghost->setPosition(1136 / 2 + 250, 640 / 2 - 150);


	auto menuu1 = Menu::create(ghost, man, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);




	sun = Sprite::createWithSpriteFrameName("sun_01.png");
	sun->setPosition(1136 / 2 - 200, 640 / 2 + 180);
	this->addChild(sun);
	sun->setScale(0.7);

	clould = Sprite::createWithSpriteFrameName("cloud.png"),

		clould->setPosition(sun->getPosition());
	this->addChild(clould);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());
		if (
			(abs(point.x - clould->getPositionX()) < clould->getContentSize().width)
			&&
			(abs(point.y - clould->getPositionY()) < clould->getContentSize().height)
			)
		{
			return true;

		}
		else
		{
			if (man->isEnabled() == true)log("111111"),
				call_bg(0);

			return false;

		}


	};



	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());


		if (ghost->getPositionX() < 500)
		{
			//	_eventDispatcher->setEnabled(false);
			return;
		}
		else
		{
			if (point.x<1136 / 2 && point.x>1136 / 2 - 400)
				clould->setPositionX(point.x);
		}


		//if (ghost->getPositionX() > 500)
		if (clould->getPositionX()<250 || clould->getPositionX()>480)//太阳出现
		{
			_eventDispatcher->setEnabled(false);
			man->setEnabled(false);
			ghost->setEnabled(false);

			man->stopAllActions();
			man_sp->stopAllActions();

			man_sp->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_hand_01.png"));


			ghost->stopAllActions();
			ghost_sp->stopAllActions();

			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("sun_01.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("sun_02.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("sun_03.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("sun_04.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("sun_05.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("sun_06.png"));
			ani1->setDelayPerUnit(3.3 / 40);
			ani1->setRestoreOriginalFrame(false);
			sun->runAction(Animate::create(ani1));

			auto ani = Animation::create();
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_01.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_02.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_03.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_04.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_05.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_06.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_07.png"));

			ani->setDelayPerUnit(2.0 / 7.0);
			ani->setRestoreOriginalFrame(false);


			ghost_sp->runAction(Animate::create(ani));

			SimpleAudioEngine::getInstance()->playEffect("sunshine_spritescreem.mp3");
			SimpleAudioEngine::getInstance()->playEffect("sunshine_smile.mp3");
			this->runAction(Sequence::create(
				DelayTime::create(2),
				CallFunc::create([=]{

				showWin();

			})

				, nullptr));






		}
	};


	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);






	this->addChild(publicMenu::createNew());

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("sunshine_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("sunshine_BG.mp3", true);

	return true;
}






void scene_12::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	_eventDispatcher->setEnabled(true);
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_12::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0023.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_12::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_12::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);





	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_12::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_12::showLose()
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	_eventDispatcher->setEnabled(true);
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_12::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0024.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_12::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_12::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}




void scene_12::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_12::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_12::createScene());
}

void scene_12::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_13::createScene());

}




void scene_12::call_man(Ref*p)
{
	man->setEnabled(false);
	ghost->setEnabled(false);

	ghost->runAction(MoveTo::create(1, Vec2(man->getPositionX() + 111, man->getPositionY())));

	auto ani11 = Animation::create();
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));

	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));

	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));


	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_02.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_03.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_04.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_05.png"));
	ani11->setDelayPerUnit(1.0 / 15);
	ani11->setRestoreOriginalFrame(false);
	ghost_sp->runAction(Animate::create(ani11));

	this->runAction(Sequence::create(DelayTime::create(1),
		CallFunc::create([=]{

		if (ghost->getPositionX() > 501)
		{
			return;
		}
		SimpleAudioEngine::getInstance()->playEffect("sunshine_screem.mp3");
		SimpleAudioEngine::getInstance()->playEffect("xie-e.mp3");
		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_death_01.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_death_02.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_death_03.png"));
		ani1->setDelayPerUnit(0.5 / 3);
		ani1->setRestoreOriginalFrame(false);
		man->stopAllActions();
		man_sp->stopAllActions();
		man_sp->runAction(Animate::create(ani1));
		man_sp->runAction(MoveBy::create(0.9, Vec2(150, 150)));
		man_sp->runAction(FadeOut::create(0.9));
		man_sp->runAction(ScaleTo::create(0.9, 0.1));

	}), DelayTime::create(1.0), CallFunc::create([=]{

		if (ghost->getPositionX() > 501)
		{
			return;
		}


		showLose();
	}), nullptr));



}



void scene_12::call_ghost(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("sunshine_spritefly.mp3");
	ghost->setEnabled(false);
	man->setEnabled(false);
	ghost->runAction(MoveTo::create(6, Vec2(man->getPositionX() + 111, man->getPositionY())));

	auto ani11 = Animation::create();
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));

	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));

	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));

	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));

	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));

	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_01.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_02.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_03.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_04.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("ghost_05.png"));
	ani11->setDelayPerUnit(6.0 / 40);
	ani11->setRestoreOriginalFrame(false);
	ghost_sp->runAction(Animate::create(ani11));

	this->runAction(Sequence::create(DelayTime::create(5.5),
		CallFunc::create([=]{

		if (ghost->getPositionX() > 501)
		{
			return;
		}
		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_death_01.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_death_02.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_death_03.png"));
		ani1->setDelayPerUnit(0.5 / 3);
		ani1->setRestoreOriginalFrame(false);
		man->stopAllActions();
		man_sp->stopAllActions();
		man_sp->runAction(Animate::create(ani1));
		man_sp->runAction(MoveBy::create(0.9, Vec2(150, 150)));
		man_sp->runAction(FadeOut::create(0.9));
		man_sp->runAction(ScaleTo::create(0.9, 0.1));
		SimpleAudioEngine::getInstance()->playEffect("sunshine_screem.mp3");
		SimpleAudioEngine::getInstance()->playEffect("xie-e.mp3");
	}), DelayTime::create(1.0), CallFunc::create([=]{

		if (ghost->getPositionX() > 501)
		{
			return;
		}
		showLose();
	}), nullptr));

}



void scene_12::call_bg(Ref*p)
{
	call_ghost(0);
}





Scene*scene_13::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_13::create();
	scene->addChild(layer);
	return scene;

}


bool scene_13::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();


	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("run_after.plist");

	bg = Sprite::createWithSpriteFrameName("bg1.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);



	bg1 = Sprite::createWithSpriteFrameName("bg2.png");
	bg1->setPosition(Vec2(1136 / 2 + 1338, 640 / 2));

	bg2 = Sprite::createWithSpriteFrameName("bg3.png");
	bg2->setPosition(Vec2(bg1->getPositionX() + 1320, 640 / 2));
	this->addChild(bg2);

	this->addChild(bg1);

	auto ss = Sprite::createWithSpriteFrameName("sign_6.png");
	ss->setPosition(1136 / 2 + 300, 205);
	ss->setAnchorPoint(Vec2(0.5, 0));
	auto ss1 = Sprite::createWithSpriteFrameName("sign_5.png");
	ss1->setPosition(1136 / 2 - 100, 205);
	ss1->setAnchorPoint(Vec2(0.5, 0));


	auto ss2 = Sprite::createWithSpriteFrameName("sign_4.png");
	ss2->setPosition(1136 / 2 + 200, 205);
	ss2->setAnchorPoint(Vec2(0.5, 0));
	auto ss11 = Sprite::createWithSpriteFrameName("sign_3.png");
	ss11->setPosition(1136 / 2 - 100, 205);
	ss11->setAnchorPoint(Vec2(0.5, 0));
	//bg->addChild(ss2);
	bg->addChild(ss11);

	bg1->addChild(ss1);

	bg1->addChild(ss);



	auto ss21 = Sprite::createWithSpriteFrameName("sign_4.png");
	ss21->setPosition(1136 / 2 + 200, 205);
	ss21->setAnchorPoint(Vec2(0.5, 0));

	auto ss111 = Sprite::createWithSpriteFrameName("sign_3.png");
	ss111->setPosition(1136 / 2 - 100, 205);
	ss111->setAnchorPoint(Vec2(0.5, 0));
	//bg->addChild(ss21);
	bg->addChild(ss111);

	bg2->addChild(ss21);

	bg2->addChild(ss111);

	auto ss1111 = Sprite::createWithSpriteFrameName("sign_5.png");
	ss1111->setPosition(1136 / 2 - 400, 205);
	ss1111->setAnchorPoint(Vec2(0.5, 0));
	bg2->addChild(ss1111);

	auto ss11111 = Sprite::createWithSpriteFrameName("sign_5.png");
	ss11111->setPosition(1136 / 2 + 400, 205);
	ss11111->setAnchorPoint(Vec2(0.5, 0));
	bg1->addChild(ss11111);


	auto ss111111 = Sprite::createWithSpriteFrameName("sign_4.png");
	ss111111->setPosition(1136 / 2 + 500, 205);
	ss111111->setAnchorPoint(Vec2(0.5, 0));
	bg->addChild(ss111111);

	auto ss1111111112 = Sprite::createWithSpriteFrameName("sign_3.png");
	ss1111111112->setPosition(1136 / 2, 205);
	ss1111111112->setAnchorPoint(Vec2(0.5, 0));
	bg1->addChild(ss1111111112);

	auto ss1111111 = Sprite::createWithSpriteFrameName("sign_2.png");
	ss1111111->setPosition(1136 / 2 - 500, 205);
	ss1111111->setAnchorPoint(Vec2(0.5, 0));
	bg1->addChild(ss1111111);


	back = MenuItemSprite::create(
		Sprite::createWithSpriteFrameName("sign_7.png"), Sprite::createWithSpriteFrameName("sign_7.png"),
		nullptr, this, menu_selector(scene_13::call_back));
	back->setPosition(1136 / 2 + 295, 640 / 2 + 20);
	auto men = Menu::create(back, nullptr);
	bg1->addChild(men);
	men->setPosition(0, 0);


	man = Sprite::createWithSpriteFrameName("man_1.png");
	man->setPosition(1136 / 2 + 150, 640 / 2 - 50);
	this->addChild(man);

	auto ani11 = Animation::create();
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_1.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_2.png"));
	ani11->setLoops(300);
	ani11->setDelayPerUnit(0.32 / 3);
	ani11->setRestoreOriginalFrame(false);
	man->runAction(Animate::create(ani11));


	dog = Sprite::createWithSpriteFrameName("dog_1.png");
	dog->setPosition(1136 / 2 - 300, 640 / 2 - 130);
	this->addChild(dog);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("dog_1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("dog_2.png"));
	ani1->setLoops(300);
	ani1->setDelayPerUnit(0.3 / 3);
	ani1->setRestoreOriginalFrame(false);
	dog->runAction(Animate::create(ani1));
	dog->runAction(MoveBy::create(5.5, Vec2(640 / 2 + 20, 0)));

	this->addChild(publicMenu::createNew());

	this->scheduleUpdate();

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("runafter_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("runafter_BG.mp3", true);
	SimpleAudioEngine::getInstance()->playEffect("runafter_dogwan.mp3");
	SimpleAudioEngine::getInstance()->playEffect("runafter_run.mp3", true);
	return true;
}



////


void scene_13::showWin()
{
	SimpleAudioEngine::getInstance()->stopAllEffects();
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0026.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);




	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_13::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_13::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);





	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_13::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_13::showLose()
{

	SimpleAudioEngine::getInstance()->stopAllEffects();
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");


	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_1::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0025.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_13::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_13::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}






void scene_13::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());


}

void scene_13::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_13::createScene());
}

void scene_13::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_14::createScene());
}




void scene_13::update(float delta)
{

	if (isBack == false)
	{
		no++;

		if (no > 360)this->unscheduleUpdate();

		bg->setPositionX(bg->getPositionX() - 7);
		bg1->setPositionX(bg1->getPositionX() - 7);
		bg2->setPositionX(bg2->getPositionX() - 7);

		if ((man->getPositionX() - dog->getPositionX()) < 112)
		{
			this->unscheduleUpdate();
			_eventDispatcher->removeAllEventListeners();
			man->stopAllActions();
			dog->stopAllActions();

			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom_2.png"));
			ani1->setLoops(5);
			ani1->setDelayPerUnit(0.2 / 3);
			ani1->setRestoreOriginalFrame(false);

			auto st = Sprite::createWithSpriteFrameName("boom.png");
			st->setPositionX(dog->getPositionX() + 110);
			st->setPositionY(dog->getPositionY() + 60);
			this->addChild(st);
			//st->runAction(Animate::create(ani1));
			SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			SimpleAudioEngine::getInstance()->stopAllEffects();
			SimpleAudioEngine::getInstance()->playEffect("runafter_dogbitman.mp3");
			st->runAction(Sequence::create(Animate::create(ani1), CallFunc::create([=]{
				this->unscheduleUpdate();
				showLose();
			}), nullptr));

		}


	}
	else
	{
		bg->setPositionX(bg->getPositionX() + 7);
		bg1->setPositionX(bg1->getPositionX() + 7);
		bg2->setPositionX(bg2->getPositionX() + 7);

	}


}



void scene_13::call_back(Ref*p)
{
	isBack = true;
	_eventDispatcher->removeAllEventListeners();
	man->stopAllActions();
	dog->stopAllActions();
	SimpleAudioEngine::getInstance()->playEffect("runafter_ya_back_run.mp3");
	auto ani11 = Animation::create();
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_3.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_4.png"));
	ani11->setLoops(300);
	ani11->setDelayPerUnit(0.32 / 3);
	ani11->setRestoreOriginalFrame(false);
	man->runAction(Animate::create(ani11));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("dog_3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("dog_4.png"));
	ani1->setLoops(300);
	ani1->setDelayPerUnit(0.3 / 3);
	ani1->setRestoreOriginalFrame(false);
	dog->runAction(Animate::create(ani1));
	man->setPositionX(man->getPositionX() + 50);

	this->runAction(Sequence::create(DelayTime::create(2.5),
		CallFunc::create([=]{
		this->unscheduleUpdate();
		man->stopAllActions();
		dog->stopAllActions();

		showWin();

	}), nullptr));
}





Scene*scene_14::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_14::create();
	scene->addChild(layer);
	return scene;

}


bool scene_14::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Boating_Texture.plist");

	auto bg = Sprite::createWithSpriteFrameName("Boating_Background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);

	flag[0] = Sprite::createWithSpriteFrameName("Boating_Boating.png");
	this->addChild(flag[0]);
	flag[0]->setPosition(1136 / 2 + 100, 640 / 2 - 30);
	flag[0]->setTag(2);

	flag[1] = Sprite::createWithSpriteFrameName("Boating_Boating.png");
	this->addChild(flag[1]);
	flag[1]->setPosition(1136 / 2 - 200, 640 / 2 - 30);
	flag[1]->setTag(1);

	flag[2] = Sprite::createWithSpriteFrameName("Boating_Boating.png");
	this->addChild(flag[2]);
	flag[2]->setPosition(1136 / 2 + 260, 640 / 2 - 250);
	flag[2]->setTag(1);

	flag[3] = Sprite::createWithSpriteFrameName("Boating_Boating.png");
	this->addChild(flag[3]);
	flag[3]->setPosition(1136 / 2, 640 / 2 - 250);
	flag[3]->setTag(2);

	flag[4] = Sprite::createWithSpriteFrameName("Boating_Boating.png");
	this->addChild(flag[4]);
	flag[4]->setPosition(1136 / 2 - 260, 640 / 2 - 250);
	flag[4]->setTag(1);


	for (int i = 0; i < 5; i++)
	{
		if (flag[i]->getTag() == 1)
		{
			flag[i]->setFlippedX(false);
		}
		else
		{
			flag[i]->setFlippedX(true);
		}
	}

	boat = Sprite::createWithSpriteFrameName("Boating_Master_1.png");
	boat->setPosition(1136 / 2 - 250, 640 / 2 - 150);
	this->addChild(boat);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Boating_Master_1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Boating_Master_2.png"));
	ani1->setDelayPerUnit(1.8 / 3.0);
	ani1->setRestoreOriginalFrame(true);
	boat->runAction(RepeatForever::create(Animate::create(ani1)));

	win = Sprite::createWithSpriteFrameName("Boating_Win.png");
	win->setPosition(Vec2(1136 / 2 + 350, 640 / 2));
	this->addChild(win);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());


		//点击 boat
		if (abs(point.x - boat->getPositionX()) < 91 && abs(point.y - boat->getPositionY()) < 97)
		{
			_eventDispatcher->setEnabled(false);

			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Boating_Master_1.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Boating_Master_3.png"));
			ani1->setLoops(3);
			ani1->setDelayPerUnit(0.4 / 3.0);
			ani1->setRestoreOriginalFrame(true);
			boat->runAction(Animate::create(ani1));
			SimpleAudioEngine::getInstance()->playEffect("sound_of_water.mp3");
			this->runAction(Sequence::create(DelayTime::create(0.8),
				CallFunc::create([=]{
				_eventDispatcher->setEnabled(true);
			}), nullptr));
			boat->runAction(MoveBy::create(0.8, Vec2(-50, 0)));
			return false;
		}



		//点击 win
		if (abs(point.x - win->getPositionX()) < 170 && abs(point.y - win->getPositionY()) < 250)
		{
			for (int i = 0; i < 5; i++)
			{
				if (flag[i]->getTag() == 2)return false;
			}
			log("ok");
			return true;
		}

		for (int i = 0; i < 5; i++)
		{

			//点击的是某一个 flag
			if (abs(point.x - flag[i]->getPositionX()) < 60 && abs(point.y - flag[i]->getPositionY()) < 50)
			{
				SimpleAudioEngine::getInstance()->playEffect("touchmove.mp3");
				if (flag[i]->getTag() == 1)
				{
					flag[i]->setFlipX(true);
					flag[i]->setTag(2);
				}
				else
				{
					flag[i]->setFlipX(false);
					flag[i]->setTag(1);
				}


			}

		}

		return false;
	};


	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		if (win->getPositionX() < 1136 / 2 - 350)
		{

			_eventDispatcher->removeAllEventListeners();
			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Boating_Master_win1.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Boating_Master_win2.png"));
			ani1->setDelayPerUnit(1.8 / 3.0);
			ani1->setRestoreOriginalFrame(false);
			boat->runAction(RepeatForever::create(Animate::create(ani1)));
			SimpleAudioEngine::getInstance()->playEffect("FingerGuess_win_huanhusheng.mp3");
			this->runAction(Sequence::create(DelayTime::create(2.0),
				CallFunc::create([=]{
				showWin();
			})
				, nullptr));

			return;
		}

		if (point.x < 1136 / 2 + 350)
		{
			win->setPositionX(point.x);
		}

	};


	listener->onTouchEnded = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		if (win->getPositionX() >= 1136 / 2 - 350)
		{
			win->setPositionX(1136 / 2 + 350);
		}

	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());
	this->scheduleUpdate();

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Boating_BG.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("Boating_BG.mp3", true);

	return true;
}






void scene_14::showWin()
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_14::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0027.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_14::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_14::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_14::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}




void scene_14::showLose()
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_14::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0028.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_14::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_14::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}



void scene_14::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_14::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_14::createScene());
}

void scene_14::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_15::createScene());

}



void scene_14::update(float delta)
{
	if (boat->getPositionX() < 0)
	{
		_eventDispatcher->removeAllEventListeners();
		this->unscheduleUpdate();
		SimpleAudioEngine::getInstance()->playEffect("FingerGuess_lose_huanhusheng.mp3");
		this->runAction(Sequence::create(DelayTime::create(2.0), CallFunc::create([=]{
			showLose(); }), nullptr));
	
	}

}





Scene*scene_15::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_15::create();
	scene->addChild(layer);
	return scene;

}


bool scene_15::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();


	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bad_weather.plist");

	auto bb = Sprite::createWithSpriteFrameName("background.png");
	bb->setPosition(1136 / 2, 640 / 2);
	this->addChild(bb);


	bgg = Sprite::createWithSpriteFrameName("background-02.png");
	bgg->setPosition(1136 / 2, 640 / 2);
	this->addChild(bgg);
	bgg->setOpacity(0);
	bg = Sprite::createWithSpriteFrameName("background-01.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	sun = Sprite::createWithSpriteFrameName("sun.png");
	sun->setPosition(1136 / 2 - 240, 640 / 2 + 200);
	this->addChild(sun);

	moon = Sprite::createWithSpriteFrameName("moon.png");
	moon->setPosition(1136 / 2 + 260, 640 / 2);
	this->addChild(moon);


	auto mount = Sprite::createWithSpriteFrameName("mountain.png");
	mount->setPosition(1136 / 2, 640 / 2);
	this->addChild(mount);

	man = Sprite::createWithSpriteFrameName("hero-01.png");
	man->setPosition(1136 / 2 - 200, 640 / 2 - 190);
	this->addChild(man);


	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-01.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-02.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-03.png"));
	ani1->setDelayPerUnit(3.0 / 10.0);
	ani1->setLoops(1);
	ani1->setRestoreOriginalFrame(false);
	man->runAction(RepeatForever::create(Animate::create(ani1)));

	lab1 = Sprite::createWithSpriteFrameName("word-01.png");
	lab1->setPosition(1136 / 2 - 80, 640 / 2 - 80);
	this->addChild(lab1);
	lab1->setOpacity(0);
	lab1->runAction(FadeIn::create(3.0));

	lab2 = Sprite::createWithSpriteFrameName("word-02.png");
	lab2->setPosition(1136 / 2 - 80, 640 / 2 - 80);
	this->addChild(lab2);
	lab2->setOpacity(0);


	Sequence* se = Sequence::create(DelayTime::create(10.0),
		CallFunc::create([=]{
		man->stopAllActions();
		lab1->runAction(FadeOut::create(1.0));
		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-die-01.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-die-02.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-die-03.png"));
		ani1->setDelayPerUnit(3.0 / 10.0);
		ani1->setLoops(1);
		ani1->setRestoreOriginalFrame(false);
		man->runAction(Animate::create(ani1));
		SimpleAudioEngine::getInstance()->playEffect("bad_weather_si.mp3");
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	}),
		DelayTime::create(1.5),
		CallFunc::create([=]{
		showLose(); })
			, nullptr);

		se->setTag(20);
		this->runAction(se);



		auto listener = EventListenerTouchOneByOne::create();
		listener->onTouchBegan = [=](Touch*touch, Event*event)
		{
			auto target = event->getCurrentTarget();
			auto point = target->convertToNodeSpace(touch->getLocation());

			if (abs(point.x - sun->getPositionX()) < 45 && abs(point.y - sun->getPositionY()) < 45)
			{
				return true;

			}

			return false;
		};


		listener->onTouchMoved = [=](Touch*touch, Event*event)
		{
			auto target = event->getCurrentTarget();
			Point point = target->convertToNodeSpace(touch->getLocation());
			log("%f", sun->getPositionY());

			if (sun->getPositionY() < 410)
			{
				sun->setPositionY(0);
				_eventDispatcher->removeAllEventListeners();
				moon->runAction(MoveTo::create(1.2, Vec2(1136 / 2 + 260, 640 / 2 + 200)));
				bg->runAction(FadeOut::create(1.0));
				bgg->runAction(FadeIn::create(1.0));

				lab1->runAction(FadeOut::create(0.8));
				lab2->runAction(FadeIn::create(1.0));

				man->stopAllActions();
				auto ani1 = Animation::create();
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-07.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-08.png"));

				ani1->setDelayPerUnit(2.0 / 10.0);
				ani1->setLoops(999);
				ani1->setRestoreOriginalFrame(false);
				man->runAction(Animate::create(ani1));
				SimpleAudioEngine::getInstance()->stopBackgroundMusic();
				SimpleAudioEngine::getInstance()->playEffect("bad_weather_bian.mp3");
				this->runAction(Sequence::create(DelayTime::create(2.0),
					CallFunc::create([=]{

					showWin();
				}),
					nullptr));

				return;
			}

			if (point.y > 640 / 2 + 30 && point.y < 642 / 2 + 200)
			{

				sun->setPositionY(point.y);
			}

		};

		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

		this->addChild(publicMenu::createNew());

		SimpleAudioEngine::getInstance()->preloadBackgroundMusic("ji-jing.mp3");

		SimpleAudioEngine::getInstance()->playBackgroundMusic("ji-jing.mp3", true);

		return true;
}





void scene_15::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_15::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0029.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_15::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_15::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_15::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_15::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_7::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0030.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_15::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_15::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}




void scene_15::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_15::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_15::createScene());
}

void scene_15::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_16::createScene());

}





Scene*scene_16::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_16::create();
	scene->addChild(layer);
	return scene;

}


bool scene_16::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("input_password.plist");

	for (int i = 0; i < 10; i++)
	{
		txt[i] = nullptr;
	}

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(1136 / 2, 640 / 2);
	this->addChild(bg);

	lay1 = Layer::create();
	lay2 = Layer::create();
	this->addChild(lay1);
	this->addChild(lay2);
	lay1->setVisible(true);
	lay2->setVisible(false);


	auto title = Sprite::createWithSpriteFrameName("title.png");
	title->setPosition(1136 / 2, 640 / 2 + 190);
	lay1->addChild(title);

	auto line = Sprite::createWithSpriteFrameName("input_box.png");
	line->setPosition(1136 / 2, 640 / 2 + 100);
	lay1->addChild(line);

	auto enter = MenuItemSprite::create(Sprite::createWithSpriteFrameName("enter.png"),
		Sprite::createWithSpriteFrameName("enter.png"), nullptr, this,
		menu_selector(scene_16::call_enter));
	enter->setPosition(1136 / 2 + 100, 640 / 2 - 250);

	auto help = MenuItemSprite::create(Sprite::createWithSpriteFrameName("help.png"),
		Sprite::createWithSpriteFrameName("help.png"), nullptr, this,
		menu_selector(scene_16::call_help));
	help->setPosition(1136 / 2 - 100, 640 / 2 - 250);

	auto pass = MenuItemSprite::create(Sprite::createWithSpriteFrameName("pass.png"),
		Sprite::createWithSpriteFrameName("pass.png"), nullptr, this,
		menu_selector(scene_16::call_pass));
	pass->setPosition(1136 / 2 + 200, 640 / 2 + 190);

	auto menu = Menu::create(enter, help, pass, nullptr);
	menu->setPosition(0, 0);
	lay1->addChild(menu);

	auto man = Sprite::createWithSpriteFrameName("hero_01.png");
	man->setPosition(1136 / 2 - 220, 640 / 2 - 90);
	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_01.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_02.png"));
	ani1->setDelayPerUnit(3.0 / 10.0);
	ani1->setLoops(1);
	ani1->setRestoreOriginalFrame(false);
	lay2->addChild(man);

	auto la = Sprite::createWithSpriteFrameName("speaking.png");
	la->setPosition(1136 / 2 + 150, 640 / 2 + 100);
	lay2->addChild(la);

	word[0] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_1.png"),
		Sprite::createWithSpriteFrameName("button_1.png"), nullptr, this,
		menu_selector(scene_16::call_word_1));
	word[0]->setPosition(1136 / 2 - 200, 640 / 2);


	word[1] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_2.png"),
		Sprite::createWithSpriteFrameName("button_2.png"), nullptr, this,
		menu_selector(scene_16::call_word_2));
	word[1]->setPosition(1136 / 2 - 100, 640 / 2);

	word[2] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_3.png"),
		Sprite::createWithSpriteFrameName("button_3.png"), nullptr, this,
		menu_selector(scene_16::call_word_3));
	word[2]->setPosition(1136 / 2, 640 / 2);

	word[3] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_4.png"),
		Sprite::createWithSpriteFrameName("button_4.png"), nullptr, this,
		menu_selector(scene_16::call_word_4));
	word[3]->setPosition(1136 / 2 + 100, 640 / 2);

	word[4] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_5.png"),
		Sprite::createWithSpriteFrameName("button_5.png"), nullptr, this,
		menu_selector(scene_16::call_word_5));
	word[4]->setPosition(1136 / 2 + 200, 640 / 2);


	word[5] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_6.png"),
		Sprite::createWithSpriteFrameName("button_6.png"), nullptr, this,
		menu_selector(scene_16::call_word_6));
	word[5]->setPosition(1136 / 2 - 200, 640 / 2 - 90);


	word[6] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_7.png"),
		Sprite::createWithSpriteFrameName("button_7.png"), nullptr, this,
		menu_selector(scene_16::call_word_7));
	word[6]->setPosition(1136 / 2 - 100, 640 / 2 - 90);

	word[7] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_8.png"),
		Sprite::createWithSpriteFrameName("button_8.png"), nullptr, this,
		menu_selector(scene_16::call_word_8));
	word[7]->setPosition(1136 / 2, 640 / 2 - 90);

	word[8] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_9.png"),
		Sprite::createWithSpriteFrameName("button_9.png"), nullptr, this,
		menu_selector(scene_16::call_word_9));
	word[8]->setPosition(1136 / 2 + 100, 640 / 2 - 90);

	word[9] = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_10.png"),
		Sprite::createWithSpriteFrameName("button_10.png"), nullptr, this,
		menu_selector(scene_16::call_word_10));
	word[9]->setPosition(1136 / 2 + 200, 640 / 2 - 90);


	auto menuu = Menu::create(word[0], word[1], word[2], word[3],
		word[4], word[5], word[6], word[7], word[8], word[9], nullptr);
	menuu->setPosition(0, 0);
	lay1->addChild(menuu);

	auto over = MenuItemSprite::create(Sprite::createWithSpriteFrameName("callover_0.png"),
		Sprite::createWithSpriteFrameName("callover_1.png"), nullptr, this,
		menu_selector(scene_16::call_over));
	over->setPosition(1136 / 2 + 200, 640 / 2 - 180);

	auto menu1 = Menu::create(over, nullptr);
	menu1->setPosition(0, 0);
	lay2->addChild(menu1);

	man->runAction(RepeatForever::create(Animate::create(ani1)));

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());


		return false;
	};


	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		Point point = target->convertToNodeSpace(touch->getLocation());

	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("input_password_background.mp3");

	SimpleAudioEngine::getInstance()->playBackgroundMusic("input_password_background.mp3", true);


	return true;
}





void scene_16::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_16::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0033.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_16::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_16::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_16::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_16::showLose()
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_16::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0032.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_16::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_16::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}





void scene_16::showOver()
{

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_16::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0031.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_16::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_16::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}




void scene_16::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}


void scene_16::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_16::createScene());
}


void scene_16::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_17::createScene());

}



void scene_16::call_enter(Ref*p)
{
	_eventDispatcher->removeAllEventListeners();
	SimpleAudioEngine::getInstance()->playEffect("input_password_boom.mp3");
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	auto st = Sprite::createWithSpriteFrameName("boom_1.png");
	lay1->addChild(st);
	st->setPosition(1136 / 2, 640 / 2);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom_1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom_2.png"));

	ani1->setDelayPerUnit(0.8 / 10.0);
	ani1->setLoops(5);
	ani1->setRestoreOriginalFrame(true);
	st->runAction(Animate::create(ani1));

	this->runAction(Sequence::create(DelayTime::create(0.8),
		CallFunc::create([=]{

		showLose();
	})
		, nullptr));
}

void scene_16::call_help(Ref*p)
{
	lay1->setVisible(false);
	lay2->setVisible(true);
	SimpleAudioEngine::getInstance()->playEffect("input_password_fake.mp3");

}



void scene_16::call_over(Ref*p)
{

	_eventDispatcher->removeAllEventListeners();
	showOver();
	/*
	auto st = Sprite::createWithSpriteFrameName("boom_1.png");
	lay2->addChild(st);
	st->setPosition(1136 / 2, 640 / 2);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom_1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom_2.png"));

	ani1->setDelayPerUnit(0.8 / 10.0);
	ani1->setLoops(5);
	ani1->setRestoreOriginalFrame(true);
	//	st->runAction(Animate::create(ani1));
	this->runAction(Sequence::create(DelayTime::create(0.8),
	CallFunc::create([=]{

	showOver();
	})
	, nullptr));
	*/
}




void scene_16::call_word_1(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_1.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(1);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_2(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_2.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(2);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_3(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_3.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(3);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_4(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_4.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(4);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_5(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_5.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(5);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_6(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_6.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(6);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_7(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_7.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(7);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_8(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_8.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(8);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_9(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_9.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			txt[i]->setTag(9);
			lay1->addChild(txt[i]);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}


void scene_16::call_word_10(Ref*p)
{
	for (int i = 0; i < 10; i++)
	{
		if (txt[i] == nullptr)
		{
			txt[i] = Sprite::createWithSpriteFrameName("word_10.png");
			txt[i]->setPosition(1136 / 2 - 225 + i * 50, 640 / 2 + 100);
			lay1->addChild(txt[i]);
			txt[i]->setTag(10);
			SimpleAudioEngine::getInstance()->playEffect("input_password_button.mp3");
			return;

		}
	}

	return;
}



void scene_16::call_pass(Ref*p)
{
	if (txt[0] == nullptr)return;
	if (txt[1] == nullptr)return;
	if (txt[2] != nullptr)return;
	if (txt[0]->getTag() == 1 && txt[1]->getTag() == 7)showWin();

}




Scene*scene_17::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_17::create();
	scene->addChild(layer);
	return scene;

}


bool scene_17::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("break_bubble.plist");

	auto bb = Sprite::createWithSpriteFrameName("background.png");
	bb->setPosition(1136 / 2, 640 / 2);
	this->addChild(bb);

	paper = Sprite::createWithSpriteFrameName("paper.png");
	paper->setPosition(1136 / 2, 640 / 2);
	this->addChild(paper);


	for (int x = 0; x < 9; x++)
	{
		for (int y = 0; y < 5; y++)
		{
			bubble[x][y] = Sprite::createWithSpriteFrameName("bubbleNormal.png");
			bubble[x][y]->setPosition(1136 / 2 - 300 + x * 73, 640 / 2 + 150 - y * 73);
			this->addChild(bubble[x][y]);
			bubble[x][y]->setTag(333);//未爆炸

		}
	}

	boom = Sprite::createWithSpriteFrameName("bomb-0.png");
	boom->setPosition(1136 / 2 + 400, 640 / 2 - 250);
	this->addChild(boom);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("broken1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("broken2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("broken1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("broken2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("broken1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("broken2.png"));

	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bubbleBreak.png"));

	ani1->setDelayPerUnit(2.0 / 20);
	ani1->setLoops(1);
	ani1->setRestoreOriginalFrame(false);

	AnimationCache::getInstance()->addAnimation(ani1, "bubble");


	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		//炸弹

		if (abs(point.x - boom->getPositionX()) < 45 && abs(point.y - boom->getPositionY()) < 60)
		{

			if (boom->getTag() != 50)
			{
				boom->setTag(50);
				auto ani1 = Animation::create();
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bomb-1.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bomb-2.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("bomb-3.png"));
				ani1->setDelayPerUnit(3.0 / 30.0);
				ani1->setLoops(8);
				ani1->setRestoreOriginalFrame(false);

				auto ani11 = Animation::create();
				ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blast-01.png"));
				ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blast-02.png"));
				ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blast-03.png"));
				ani11->setDelayPerUnit(3.0 / 30.0);
				ani11->setLoops(3);
		
				ani11->setRestoreOriginalFrame(false);

				boom->runAction(Sequence::create(Animate::create(ani1),
					CallFunc::create([=]{
					SimpleAudioEngine::getInstance()->playEffect("boom.mp3");
					if (abs(1136 / 2 - boom->getPositionX()) < 370 && abs(640 / 2 - boom->getPositionY()) < 225)
					{
						_eventDispatcher->setEnabled(false);
						for (int x = 0; x < 9; x++)
						{
							for (int y = 0; y < 5; y++)
							{
								bubble[x][y]->setVisible(false);

							}
						}
						paper->setVisible(false);
						return;
					}

					paper->setTag(50);

				}), Animate::create(ani11), CallFunc::create([=]{

					if (abs(1136 / 2 - boom->getPositionX()) < 370 && abs(640 / 2 - boom->getPositionY()) < 225)
					{
						boom->setVisible(false);
						showWin();

					}
					boom->setVisible(false);
					_eventDispatcher->setEnabled(true);

				}), nullptr));

				return true;

			}

			return true;
		}

		//泡泡
		for (int x = 0; x < 9; x++)
		{
			for (int y = 0; y < 5; y++)
			{
				//点击泡泡
				if (abs(point.x - bubble[x][y]->getPositionX()) < 30 && abs(point.y - bubble[x][y]->getPositionY()) < 30)
				{
					if (bubble[x][y]->getTag() == 0)return false;//已经爆裂

					no++;
					if (no >= 45)showLose();
					SimpleAudioEngine::getInstance()->playEffect("break_bubble_boom.mp3");
					bubble[x][y]->runAction(Animate::create(AnimationCache::getInstance()->getAnimation("bubble")));
					bubble[x][y]->setTag(0);//已经爆炸

					return false;

				}
			}
		}
		return false;
	};


	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		Point point = target->convertToNodeSpace(touch->getLocation());

		//肯定是点击的炸弹
		//	if (boom->)

		if (paper->getTag() == 50)return;

		if ((point.x < 1130 || point.x > 6) &&
			(point.y<634 || point.y > 6))
			boom->setPosition(point);

	};


	auto lab = Sprite::createWithSpriteFrameName("remind.png");
	lab->setPosition(1136 / 2, 640 / 2);
	this->addChild(lab);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	_eventDispatcher->setEnabled(false);
	lab->runAction(Sequence::create(DelayTime::create(2.0),
		CallFunc::create([=]{
		_eventDispatcher->setEnabled(true); }),
			FadeOut::create(0.8)
			, nullptr));

		this->addChild(publicMenu::createNew());

		SimpleAudioEngine::getInstance()->preloadBackgroundMusic("break_bubble_background.mp3");

		SimpleAudioEngine::getInstance()->playBackgroundMusic("break_bubble_background.mp3", true);




		return true;
}





void scene_17::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	_eventDispatcher->setEnabled(true);
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_17::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0034.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_17::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_17::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_17::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_17::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_7::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0035.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_17::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_17::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}



void scene_17::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_17::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_17::createScene());
}

void scene_17::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_18::createScene());

}







Scene*scene_18::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_18::create();
	scene->addChild(layer);
	return scene;

}


bool scene_18::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("send_he_packing.plist");

	auto bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	menu = Sprite::createWithSpriteFrameName("bulletMenu.png");
	this->addChild(menu);
	menu->setPosition(1136 / 2 + 320, 640 / 2 + 120);

	gun = Sprite::createWithSpriteFrameName("gun.png");
	gun->setPosition(Vec2(1136 / 2 - 350, 640 / 2 - 175));
	this->addChild(gun);


	letFly = MenuItemSprite::create(Sprite::createWithSpriteFrameName("slowBullet.png"),
		Sprite::createWithSpriteFrameName("slowBullet.png"),
		nullptr
		, this, menu_selector(scene_18::call_letFly));

	letFly->setPosition(menu->getPositionX() + 800, menu->getPositionY());
	auto menuu11 = Menu::create(letFly, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	man = Sprite::createWithSpriteFrameName("hero-01.png");
	man->setPosition(1136 / 2 - 100, 640 / 2 - 172);
	this->addChild(man);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-01.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-02.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(1);
	ani1->setRestoreOriginalFrame(false);
	man->runAction(RepeatForever::create(Animate::create(ani1)));

	man->runAction(RepeatForever::create(Sequence::create(MoveBy::create(3.0, Vec2(250, 0)),
		CallFunc::create([=]{
		man->setFlippedX(true);
	}), MoveBy::create(3.0, Vec2(-250, 0)),
		CallFunc::create([=]{
		man->setFlippedX(false);
	}),
		nullptr)));


	bullet = Sprite::createWithSpriteFrameName("bullet.png");
	bullet->setPosition(1136 / 2 - 220, 640 / 2 - 120);
	this->addChild(bullet);
	bullet->setTag(2);//默认正常模式
	bullet->setVisible(false);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		//点击menu
		if (abs(point.x - menu->getPositionX()) < 55 && abs(point.y - menu->getPositionY()) < 35)
		{
			log("11");
			return true;
		}

		//点击枪
			if (abs(point.x - gun->getPositionX()) < 135 && abs(point.y - gun->getPositionY()) < 75)
			{
				man->stopAllActions();
				man->setFlippedX(false);
				man->setTag(10);
				SimpleAudioEngine::getInstance()->playEffect("send_he_packing_gun.mp3");
				auto ani1 = Animation::create();
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-01.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-02.png"));

				ani1->setDelayPerUnit(2.8 / 20);
				ani1->setLoops(1);
				ani1->setRestoreOriginalFrame(false);
				man->runAction(RepeatForever::create(Animate::create(ani1)));

				this->scheduleUpdate();
				bullet->setVisible(true);
				return false;
			}


		return false;
	};


	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		if (point.x > 0 && point.x < 1100)
		{
			menu->setPositionX(point.x);
			letFly->setPositionX(point.x + 800);
		}

	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("send_he_packing_background.mp3");

	SimpleAudioEngine::getInstance()->playBackgroundMusic("send_he_packing_background.mp3", true);




	return true;
}





void scene_18::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_18::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);

	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0037.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);

	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_18::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);

	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_18::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_18::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);

}




void scene_18::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_18::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0036.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_18::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_18::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}




void scene_18::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_18::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_18::createScene());
}

void scene_18::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_19::createScene());

}




void  scene_18::update(float delta)
{

	//控制子弹速度
	if (bullet->getTag() == 2)//正常模式

	{
		bullet->setPositionX(bullet->getPositionX() + 10);
	}
	else if (bullet->getTag() == 3)//减速模式
	{
		bullet->setPositionX(bullet->getPositionX() + 1);
	}

	if (man->getTag() == 10)
	{
		man->setPositionX(man->getPositionX() + 4);
	}

	if (abs(man->getPositionX() - bullet->getPositionX()) < 40)
	{
		//die

		_eventDispatcher->removeAllEventListeners();
		this->unscheduleUpdate();
		man->stopAllActions();
		man->setTag(5);
		bullet->setVisible(false);
		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-01.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-02.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-03.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-04.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-05.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-06.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-06.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-06.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-06.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-06.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-06.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero-blood-06.png"));

		ani1->setDelayPerUnit(0.8 / 20);
		ani1->setLoops(1);
		ani1->setRestoreOriginalFrame(false);

		man->runAction(Sequence::create(Animate::create(ani1), CallFunc::create([=]{
			showLose();
		}), nullptr));

	}

	if (man->getPositionX() > 1190)
	{
		//win
		_eventDispatcher->removeAllEventListeners();
		this->unscheduleUpdate();
		showWin();

	}
}



void scene_18::call_letFly(Ref*p)
{

	if (bullet->isVisible() == true)
		bullet->setTag(3);
	else
		return;

}






Scene*scene_19::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_19::create();
	scene->addChild(layer);
	return scene;

}



bool scene_19::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("now_time.plist");

	auto bg = Sprite::createWithSpriteFrameName("background_16.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);

	auto lab1 = Sprite::createWithSpriteFrameName("word_01.png");
	lab1->setPosition(1136 / 2 - 100, 640 / 2 - 10);
	this->addChild(lab1);
	lab1->setOpacity(0);

	auto lab2 = Sprite::createWithSpriteFrameName("word_02.png");
	lab2->setPosition(1136 / 2 - 300, 640 / 2 + 20);
	this->addChild(lab2);
	lab2->setOpacity(0);

	lab1->runAction(FadeIn::create(2.0));

	this->runAction(Sequence::create(DelayTime::create(3.0),
		CallFunc::create([=]{
		lab2->runAction(FadeIn::create(2.0));
	}), nullptr));

	auto t1 = Sprite::createWithSpriteFrameName("minute_hand.png");
	t1->setPosition(1136 / 2 - 225, 640 / 2 + 138);
	this->addChild(t1);
	t1->setAnchorPoint(Vec2(0, 1));


	auto t11 = Sprite::createWithSpriteFrameName("hour_hand.png");
	t11->setPosition(1136 / 2 - 225, 640 / 2 + 138);
	this->addChild(t11);
	t11->setAnchorPoint(Vec2(0, 0));

	t11->setRotation(45);
	t1->runAction(RepeatForever::create(RotateBy::create(2.0, 360)));
	t11->runAction(RepeatForever::create(RotateBy::create(2.0 * 12, 360)));

	char str[10];

	for (int i = 0; i < 4; i++)
	{
		sprintf(str, "%d", i + 1);
		Num[i] = Label::createWithTTF(str, "vssetup.ttf", 45);
		Num[i]->setPosition(1136 / 2 + 87 + i * 60, 640 / 2 - 90);
		this->addChild(Num[i]);
		Num[i]->setTag(i + 1);
	}

	for (int i = 4; i < 8; i++)
	{
		sprintf(str, "%d", i + 1);
		Num[i] = Label::createWithTTF(str, "vssetup.ttf", 45);
		Num[i]->setPosition(1136 / 2 + 87 + (i - 4) * 60, 640 / 2 - 140);
		this->addChild(Num[i]);
		Num[i]->setTag(i + 1);
	}


	Num[8] = Label::createWithTTF("9", "vssetup.ttf", 45);
	Num[8]->setPosition(1136 / 2 + 87 + 0 * 60, 640 / 2 - 192);
	this->addChild(Num[8]);
	Num[8]->setTag(9);

	Num[9] = Label::createWithTTF("0", "vssetup.ttf", 45);
	Num[9]->setPosition(1136 / 2 + 87 + 1 * 60, 640 / 2 - 192);
	this->addChild(Num[9]);
	Num[9]->setTag(10);

	Num[10] = Label::createWithTTF("C", "vssetup.ttf", 45);
	Num[10]->setPosition(1136 / 2 + 87 + 2 * 60, 640 / 2 - 192);
	this->addChild(Num[10]);
	Num[10]->setTag(11);

	Num[11] = Label::createWithTTF("ok", "vssetup.ttf", 40);
	Num[11]->setPosition(1136 / 2 + 87 + 3 * 60, 640 / 2 - 192);
	this->addChild(Num[11]);
	Num[11]->setTag(12);


	lab[0] = Label::createWithTTF("-", "vssetup.ttf", 40);
	lab[0]->setPosition(1136 / 2 + 65, 640 / 2);
	this->addChild(lab[0]);
	lab[0]->setTag(0);


	lab[1] = Label::createWithTTF("-", "vssetup.ttf", 40);
	lab[1]->setPosition(1136 / 2 + 135, 640 / 2);
	this->addChild(lab[1]);
	lab[1]->setTag(0);


	lab[2] = Label::createWithTTF("-", "vssetup.ttf", 40);
	lab[2]->setPosition(1136 / 2 + 218, 640 / 2);
	this->addChild(lab[2]);
	lab[2]->setTag(0);


	lab[3] = Label::createWithTTF("-", "vssetup.ttf", 40);
	lab[3]->setPosition(1136 / 2 + 218 + 70, 640 / 2);
	this->addChild(lab[3]);
	lab[3]->setTag(0);


	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		for (int i = 0; i < 12; i++)
		{
			if (abs(point.x - Num[i]->getPositionX()) < 30 && abs(point.y - Num[i]->getPositionY()) < 30)
			{
				SimpleAudioEngine::getInstance()->playEffect("now_time_button.mp3");
				char str[10];

				if (Num[i]->getTag() == 11)
				{// c
					if (no <= 0)return false;

					no--;
					sprintf(str, "-");
					lab[no]->setString("-");
			

					return false;
				}


				if (Num[i]->getTag() == 12)
				{//ok

					if (no < 4)
					{
						_eventDispatcher->removeAllEventListeners();
						auto boom = Sprite::createWithSpriteFrameName("blow_up_03.png");
						this->addChild(boom);
						boom->setPosition(1136 / 2 + 200, 640 / 2 - 150);

						auto ani1 = Animation::create();
						ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_01.png"));
						ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_02.png"));
						ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_03.png"));
						ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_04.png"));

						ani1->setDelayPerUnit(1.8 / 20);
						ani1->setLoops(3);
						ani1->setRestoreOriginalFrame(false);
						SimpleAudioEngine::getInstance()->playEffect("now_time_zadang.mp3");
						boom->runAction(Sequence::create(Animate::create(ani1), CallFunc::create([=]{
							boom->setVisible(false);
							showLose();

						}), nullptr));

					}
					else
					{
						time_t tt;
						time(&tt);
						struct tm * now;
						now = localtime(&tt);

						String ss;
						for (int i = 0; i < 4; i++)
						{
							ss.append(lab[i]->getString());

						}

						char str1[20];
				

						sprintf(str1, "%d%d%d%d", now->tm_hour / 10, now->tm_hour % 10, now->tm_min / 10, now->tm_min % 10);

						if (strcmp(ss.getCString(), str1) == 0)
						{
							SimpleAudioEngine::getInstance()->playEffect("now_time_win.mp3");
							_eventDispatcher->removeAllEventListeners();
							this->runAction(Sequence::create(DelayTime::create(1.5), CallFunc::create([=]{
							
								showWin();
							}),nullptr));
						
						}
						else
						{

							_eventDispatcher->removeAllEventListeners();
							auto boom = Sprite::createWithSpriteFrameName("blow_up_03.png");
							this->addChild(boom);
							boom->setPosition(1136 / 2 + 200, 640 / 2 - 150);

							auto ani1 = Animation::create();
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_01.png"));
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_02.png"));
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_03.png"));
							ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("blow_up_04.png"));

							ani1->setDelayPerUnit(1.8 / 20);
							ani1->setLoops(3);
							ani1->setRestoreOriginalFrame(false);
							SimpleAudioEngine::getInstance()->playEffect("now_time_zadang.mp3");
							boom->runAction(Sequence::create(Animate::create(ani1), CallFunc::create([=]{
								boom->setVisible(false);
								showLose();

							}), nullptr));

						}
					}

				}

				if (no >= 4)return false;

				sprintf(str, "%d", Num[i]->getTag() % 10);
				lab[no]->setString(str);
				no++;

				return false;
			}
		}

		return false;
	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("now_time_shijian.mp3");

	SimpleAudioEngine::getInstance()->playBackgroundMusic("now_time_shijian.mp3", true);




	return true;

}



void scene_19::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_19::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));



	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0038.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);



	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_19::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);



	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_19::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_19::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_19::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_19::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0039.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_19::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_19::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);



}




void scene_19::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_19::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_19::createScene());
}

void scene_19::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_20::createScene());

}







Scene*scene_20::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_20::create();
	scene->addChild(layer);
	return scene;

}


bool scene_20::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Knife_Texture.plist");

	auto bb = Sprite::createWithSpriteFrameName("Master_bg.png");
	bb->setPosition(1136 / 2, 640 / 2);
	this->addChild(bb);


	man = Sprite::createWithSpriteFrameName("Master_Normal.png");
	man->setPosition(1136 / 2 - 300, 640 / 2 - 85);
	this->addChild(man);

	bad = Sprite::createWithSpriteFrameName("man_Work_1.png");
	bad->setPosition(1136 / 2 + 450, 640 / 2 - 110);
	this->addChild(bad);



	knife = Sprite::createWithSpriteFrameName("knife.png");
	knife->setPosition(1136 / 2 - 20, 640 / 2 - 240);
	this->addChild(knife);



	bad->runAction(MoveTo::create(3, Vec2(1136 / 2 + 50, 640 / 2 - 110)));


	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Work_1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Work_2.png"));

	ani1->setDelayPerUnit(2.0 / 20);
	ani1->setLoops(1);
	ani1->setRestoreOriginalFrame(false);
	bad->runAction(RepeatForever::create(Animate::create(ani1)));


	police = Sprite::createWithSpriteFrameName("policeman_1.png");
	this->addChild(police);
	police->setPosition(-70, 640 / 2 - 135);














	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		if (man->getTag() == 3 || man->getTag() == 5)return false;
		man->stopAllActions();
		man->setTag(3);
		auto ani1 = Animation::create();
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Work_1.png"));
		ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Work_2.png"));
		ani1->setDelayPerUnit(3.0 / 20);
		ani1->setLoops(1);
		ani1->setRestoreOriginalFrame(true);
		man->runAction(RepeatForever::create(Animate::create(ani1)));

		man->runAction(MoveTo::create(2, Vec2(1136 / 2 + 50, 640 / 2 - 110)));


		return false;
	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);


	this->addChild(publicMenu::createNew());
	this->scheduleUpdate();
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("PK_With_Knife_BG.mp3");

	SimpleAudioEngine::getInstance()->playBackgroundMusic("PK_With_Knife_BG.mp3", true);




	return true;
}





void scene_20::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");
	_eventDispatcher->setEnabled(true);
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_20::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0042.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_20::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_20::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_20::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_20::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_20::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0041.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_20::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_20::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}



void scene_20::showPolice()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_20::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0040.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_20::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_20::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}


void scene_20::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_20::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_20::createScene());
}

void scene_20::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_21::createScene());

}






void scene_20::update(float delta)
{

	if (this->getTag() == 22)//警察来了
	{
		kk++;

		time_t tt;
		time(&tt);
		struct tm * now;
		now = localtime(&tt);
		int w = 0;
		nowdate = now->tm_hour * 10000 + now->tm_min * 100 + now->tm_sec;

		if (kk >= 30)
		{
			kk = 0;
			if (nowdate - UserDefault::getInstance()->getIntegerForKey("data") > 2)
			{
				this->unscheduleUpdate();
				_eventDispatcher->removeAllEventListeners();
				man->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Knife_4.png"));
				this->runAction(Sequence::create(DelayTime::create(1.5),
					CallFunc::create([=]{
					_eventDispatcher->removeAllEventListeners();
					police->stopAllActions();
					man->stopAllActions();
					this->stopAllActions();

					showWin();
				}), nullptr));

			}

			time_t tt;
			time(&tt);
			struct tm * now;
			now = localtime(&tt);
			int w = 0;
			w = now->tm_hour * 10000 + now->tm_min * 100 + now->tm_sec;
			log("%d", w);
			UserDefault::getInstance()->setIntegerForKey("data", w);

		}

	}
	else
	{

		if (bad->getTag() != 4)a++;

		if (man->getTag() != 5)
		if (a >= 180)//敌人走到刀下
		{
			a = 0;
			//man->stopAllActions();
			bad->stopAllActions();
			bad->setTag(4);
			//拿刀继续走
			man->setTag(3);

			knife->setVisible(false);

			auto ani11 = Animation::create();
			ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Stoop_1.png"));
			ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Stoop_2.png"));
			ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Stoop_3.png"));
			ani11->setDelayPerUnit(1.0 / 20);
			ani11->setLoops(1);
			ani11->setRestoreOriginalFrame(false);

			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Knife_1.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Knife_2.png"));
			ani1->setDelayPerUnit(2.0 / 20);
			ani1->setLoops(99999);
			ani1->setRestoreOriginalFrame(false);

			bad->runAction(Sequence::create(Animate::create(ani11),
				Animate::create(ani1), nullptr));
			bad->runAction(MoveBy::create(5, Vec2(-700, 0)));

		}


		if (bad->getTag() == 4)//bad拿着刀
		{
			if (abs(man->getPositionX() - bad->getPositionX()) < 100)
			{
				this->unscheduleUpdate();
				_eventDispatcher->removeAllEventListeners();
				bad->stopAllActions();
				man->stopAllActions();


				auto ani2 = Animation::create();
				ani2->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Died_1.png"));
				ani2->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Died_2.png"));
				ani2->setDelayPerUnit(0.05);
				ani2->setLoops(1);
				ani2->setRestoreOriginalFrame(false);
				SimpleAudioEngine::getInstance()->playEffect("PK_With_Knife_stranger.mp3");
				man->runAction(Sequence::create(Animate::create(ani2), DelayTime::create(1),
					CallFunc::create([=]{

					showLose();
				}), nullptr));



			}
			return;

		}


		if (man->getTag() == 3)aa++;//man 开始走
		if (aa >= 70)//走到刀下
		{
			aa = 0;
			man->setTag(5);

			man->stopAllActions();
			knife->setVisible(false);
			auto ani11 = Animation::create();
			ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Stoop_1.png"));
			ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Stoop_2.png"));
			ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Stoop_1.png"));
			ani11->setDelayPerUnit(0.05);
			ani11->setLoops(1);
			ani11->setRestoreOriginalFrame(false);

			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Knife_1.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Knife_2.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Knife_3.png"));

			ani1->setDelayPerUnit(2.5 / 20);
			ani1->setLoops(99999);
			ani1->setRestoreOriginalFrame(false);

			man->runAction(Sequence::create(Animate::create(ani11),
				Animate::create(ani1), nullptr));
			man->runAction(MoveBy::create(7, Vec2(1190, 0)));

		}

		if (abs(man->getPositionX() - bad->getPositionX()) < 100)
		{
			this->unscheduleUpdate();

			bad->stopAllActions();
			man->stopAllActions();

			man->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Imprision.png"));

			auto lab = Sprite::createWithSpriteFrameName("talk.png");
			lab->setPosition(man->getPositionX() + 180, man->getPositionY() + 100);
			this->addChild(lab);

			auto ani2 = Animation::create();
			ani2->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Died_1.png"));
			ani2->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Died_2.png"));
			ani2->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("man_Died_3.png"));

			ani2->setDelayPerUnit(2.0 / 30);
			ani2->setLoops(1);
			ani2->setRestoreOriginalFrame(false);
			bad->runAction(Sequence::create(Animate::create(ani2),
				CallFunc::create([=]{
				//	_eventDispatcher->removeAllEventListeners();
				this->setTag(22);
				man->stopAllActions();
				bad->stopAllActions();
				this->stopAllActions();

				time_t tt;
				time(&tt);
				struct tm * now;
				now = localtime(&tt);
				int w = 0;
				w = now->tm_hour * 10000 + now->tm_min * 100 + now->tm_sec;
				UserDefault::getInstance()->setIntegerForKey("data", w);
				SimpleAudioEngine::getInstance()->playEffect("PK_With_Knife_master.mp3");
				this->runAction(Sequence::create(DelayTime::create(5.0),
					CallFunc::create([=]{
					this->unscheduleUpdate();
					SimpleAudioEngine::getInstance()->playEffect("PK_With_Knife_police.mp3");
					auto ani11 = Animation::create();
					ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("policeman_1.png"));
					ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("policeman_2.png"));
					//	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("policeman_3.png"));
					ani11->setDelayPerUnit(3.0 / 20);
					ani11->setLoops(9999);
					ani11->setRestoreOriginalFrame(false);
					auto zz = Animate::create(ani11);
					zz->setTag(3);
					police->runAction(zz);
					police->runAction(Sequence::create(
						MoveTo::create(3.0, Vec2(man->getPositionX() - 180, police->getPositionY())),
						CallFunc::create([=]{
						police->stopActionByTag(3);
						police->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("policeman_3.png"));

					}), DelayTime::create(1.5), CallFunc::create([=]{
						_eventDispatcher->removeAllEventListeners();
						this->unscheduleUpdate();
						showPolice();
					}),
						nullptr));

					//	

				}), nullptr));
				this->scheduleUpdate();



			}), nullptr));

		}

	}
}






Scene*scene_21::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_21::create();
	scene->addChild(layer);
	return scene;

}


bool scene_21::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("car_pass_by_Road_difficult.plist");

	bg = Sprite::createWithSpriteFrameName("background.png");
	bg->setPosition(1136 / 2, 640 / 2);
	this->addChild(bg);
	bg->setTag(9);




	pause = MenuItemSprite::create(Sprite::createWithSpriteFrameName("stop.png"),
		Sprite::createWithSpriteFrameName("stop.png"), nullptr, this,
		menu_selector(scene_21::call_pause));

	auto men = Menu::create(pause, nullptr);

	pause->setPosition(1136 / 2 + 400, 640 / 2 - 90);
	men->setPosition(0, 0);
	this->addChild(men);


	rock = Sprite::createWithSpriteFrameName("stone.png");
	rock->setPosition(1136 / 2, 640 / 2 - 85);
	this->addChild(rock);
	rock->setTag(10);//未移开

	car = Sprite::createWithSpriteFrameName("exhaust-00.png");
	car->setPosition(1136 / 2 - 430, 640 / 2 - 82);
	this->addChild(car);




	car->setTag(5);//stop  9 is run



	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{

		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		//点击卡车
		if (abs(point.x - car->getPositionX()) < 78 && abs(point.y - car->getPositionY()) < 53)
		{
			
			log("7079");
			if (car->getTag() == 5)
			{
				car->setTag(9);
				auto ani1 = Animation::create();
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("exhaust-02.png"));
				ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("exhaust-03.png"));
				SimpleAudioEngine::getInstance()->playEffect("car_passby_road_difficult_kaiche.mp3");
				ani1->setDelayPerUnit(2.0 / 20);
				ani1->setLoops(1);
				ani1->setRestoreOriginalFrame(true);
				car->runAction(RepeatForever::create(Animate::create(ani1)));
				return false;
			}

			if (car->getTag() == 9)
			{
				car->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("exhaust-00.png"));
				car->setTag(5);
				car->stopAllActions();
				return false;
			}

			return false;

		}




		if (abs(point.x - rock->getPositionX()) < 58 && abs(point.y - rock->getPositionY()) < 54)
		{
			return true;

		}

		return false;
	};

	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{

		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());
		if (point.y < 340 && point.y>(640 / 2 - 85))
		{

			rock->setPositionY(point.y);
		}

	};

	listener->onTouchEnded = [=](Touch*touch, Event*event)
	{
		rock->runAction(MoveTo::create(0.3, Vec2(1136 / 2, 640 / 2 - 85)));
		rock->runAction(Sequence::create(DelayTime::create(0.3),
			CallFunc::create([=]{
			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("background.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("background-01.png"));
			SimpleAudioEngine::getInstance()->playEffect("car_passby_road_difficult_shitoudiaolou.mp3");
			ani1->setDelayPerUnit(2.0 / 20);
			ani1->setLoops(4);
			ani1->setRestoreOriginalFrame(true);
			bg->runAction(Animate::create(ani1));
		})
			, nullptr));

	};

	//重力
	Device::setAccelerometerEnabled(true);
	auto listeneracc = EventListenerAcceleration::create([=](Acceleration* acc, Event* event){
		if (bg->getTag() != 11)
		{

			gra_x = acc->x;
			if (gra_x > 0.7)
			{
				bg->setTag(11);

				this->schedule([=](...){
					rock->runAction(MoveBy::create(4, Vec2(1300, 0)));
					rock->runAction(RotateBy::create(4, 360 * 5));
					this->unschedule("gra");
					SimpleAudioEngine::getInstance()->playEffect("car_passby_road_difficult_feishitou.mp3");
				}, 5, "gra");

			}

		}
		if (gra_x <= 0.7)
		{
			bg->setTag(10);
			this->unschedule("gra");
		}

		log("x=%f,y=%f,z=%f", acc->x, acc->y, acc->z);
	});
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listeneracc, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());
	this, scheduleUpdate();
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("huaji-kongbu.mp3");

	SimpleAudioEngine::getInstance()->playBackgroundMusic("huaji-kongbu.mp3", true);


	return true;
}





void scene_21::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");

	_eventDispatcher->setEnabled(true);
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_21::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0044.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_21::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_21::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_21::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_21::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_21::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0043.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_21::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_21::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}





void scene_21::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_21::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_21::createScene());
}

void scene_21::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_22::createScene());

}




void scene_21::call_pause(Ref*p)
{
	if (car->getPositionX() > (pause->getPositionX() + 50))return;

	if (abs(car->getPositionX() - pause->getPositionX()) < 100)
	{
		this->unscheduleUpdate();
		_eventDispatcher->removeAllEventListeners();
		showWin();
	}

}

void scene_21::update(float delta)
{
	if (car->getTag() == 9)
	{
		car->setPositionX(car->getPositionX() + 2);

		if (rock->getTag() == 10)//石头未移开
		if (abs(car->getPositionX() - rock->getPositionX()) < 165
			&&
			abs(car->getPositionY() - rock->getPositionY()) < 53
			)//撞石头
		{
			this->unscheduleUpdate();
			_eventDispatcher->removeAllEventListeners();
			SimpleAudioEngine::getInstance()->playEffect("boom.mp3");
			car->stopAllActions();
			car->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("exhaust-00.png"));

			auto boom = Sprite::createWithSpriteFrameName("boom-01.png");
			boom->setPosition(Vec2(car->getPositionX() + 120, car->getPositionY()));

			this->addChild(boom);

			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom-01.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom-02.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom-03.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom-04.png"));
			ani1->setDelayPerUnit(2.0 / 20);
			ani1->setLoops(1);
			ani1->setRestoreOriginalFrame(false);
			boom->runAction(Animate::create(ani1));
			car->runAction(MoveBy::create(0.1, Vec2(-100, 0)));

			boom->runAction(MoveBy::create(0.1, Vec2(-100, 0)));

			rock->runAction(MoveBy::create(1.0, Vec2(100, 0)));
			rock->runAction(RotateBy::create(1.0, 360));
			this->runAction(Sequence::create(DelayTime::create(1.5), CallFunc::create([=]{

				showLose();
			}), nullptr));

		}

		if (car->getPositionX() > 1200)
		{
			this->unscheduleUpdate();
			_eventDispatcher->removeAllEventListeners();

			car->stopAllActions();
			car->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("exhaust-00.png"));

			auto boom = Sprite::createWithSpriteFrameName("boom-01.png");
			boom->setPosition(Vec2(car->getPositionX() + 120, car->getPositionY()));
			SimpleAudioEngine::getInstance()->playEffect("boom.mp3");
			this->addChild(boom);

			auto ani1 = Animation::create();
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom-01.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom-02.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom-03.png"));
			ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("boom-04.png"));
			ani1->setDelayPerUnit(2.0 / 20);
			ani1->setLoops(1);
			ani1->setRestoreOriginalFrame(false);
			boom->runAction(Animate::create(ani1));
			car->runAction(MoveBy::create(0.1, Vec2(-100, 0)));

			boom->runAction(MoveBy::create(0.1, Vec2(-100, 0)));


			this->runAction(Sequence::create(DelayTime::create(1.5), CallFunc::create([=]{

				showLose();
			}), nullptr));
		}


	}




}





Scene*scene_22::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_22::create();
	scene->addChild(layer);
	return scene;

}


bool scene_22::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("FingerGuess_Texture.plist");

	auto bg = Sprite::createWithSpriteFrameName("FingerGuess_background.png");
	bg->setPosition(1136 / 2, 640 / 2);
	this->addChild(bg);

	fig_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("FingerGuess_shear.png"),
		Sprite::createWithSpriteFrameName("FingerGuess_shear.png"),
		nullptr
		, this, menu_selector(scene_22::call_fig_1));

	fig_1->setPosition(Vec2(1136 / 2 - 120, 640 / 2 + 170));

	fig_2 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("FingerGuess_stone.png"),
		Sprite::createWithSpriteFrameName("FingerGuess_stone.png"),
		nullptr
		, this, menu_selector(scene_22::call_fig_2));

	fig_2->setPosition(Vec2(1136 / 2, 640 / 2 + 170));

	fig_3 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("FingerGuess_cloth.png"),
		Sprite::createWithSpriteFrameName("FingerGuess_cloth.png"),
		nullptr
		, this, menu_selector(scene_22::call_fig_3));

	fig_3->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 170));


	enter = MenuItemSprite::create(Sprite::createWithSpriteFrameName("FingerGuess_ensure.png"),
		Sprite::createWithSpriteFrameName("FingerGuess_ensure.png"),
		nullptr
		, this, menu_selector(scene_22::call_enter));

	enter->setPosition(Vec2(1136 / 2, 640 / 2));


	auto menuu1 = Menu::create(fig_1, fig_2, fig_3, enter, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);

	enter->setEnabled(false);

	fig_my = Sprite::createWithSpriteFrameName("FingerGuess_BIG_2.png");
	fig_my->setPosition(1136 / 2 - 260, 640 / 2 - 80);
	this->addChild(fig_my);


	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_2.png"));

	ani1->setDelayPerUnit(5.0 / 20);
	ani1->setLoops(2);
	ani1->setRestoreOriginalFrame(true);
	fig_my->runAction(RepeatForever::create(Animate::create(ani1)));


	fig_enemy = Sprite::createWithSpriteFrameName("FingerGuess_BIG_2.png");
	fig_enemy->setPosition(1136 / 2 + 260, 640 / 2 - 80);
	this->addChild(fig_enemy);


	auto ani11 = Animation::create();
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_1.png"));
	ani11->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_2.png"));

	ani11->setDelayPerUnit(5.0 / 20);
	ani11->setLoops(2);
	ani11->setRestoreOriginalFrame(true);
	fig_enemy->runAction(RepeatForever::create(Animate::create(ani11)));

	fig_enemy->setFlippedX(true);


























	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());
		if (enter->isEnabled() == false)return false;

		if (abs(point.x - fig_my->getPositionX()) < 160 && abs(point.y - fig_my->getPositionY()) < 140)
		{
			return true;
		}

		return false;
	};

	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		if (point.x > 50 && point.x < (1136 - 100))
		{
			fig_my->setPositionX(point.x);
		}

	};

	listener->onTouchEnded = [=](...)
	{
		if (fig_my->getPositionX()>(1136 / 2))
		{
			fig_my->setPositionX(1136 / 2 + 260);
			fig_my->setFlippedX(true);
			fig_enemy->setFlippedX(false);
			fig_enemy->setPositionX(1136 / 2 - 260);

		}
		else
		{

			fig_my->setPositionX(1136 / 2 - 260);
			fig_my->setFlippedX(false);
			fig_enemy->setFlippedX(true);
			fig_enemy->setPositionX(1136 / 2 + 260);

		}


	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("FingerGuess_BG.mp3");

	SimpleAudioEngine::getInstance()->playBackgroundMusic("FingerGuess_BG.mp3", true);


	return true;
}





void scene_22::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");

	_eventDispatcher->setEnabled(true);
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_22::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0046.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_22::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_22::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_22::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_22::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_22::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0045.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_22::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_22::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}





void scene_22::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_22::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_22::createScene());
}

void scene_22::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_23::createScene());

}







void scene_22::call_enter(Ref*p)
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("show.mp3");
	fig_enemy->stopAllActions();
	enter->setEnabled(false);
	_eventDispatcher->removeAllEventListeners();
	switch (who)
	{
	case 1:
	{
			  fig_enemy->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_stone.png"));

	}break;

	case 2:
	{

			  fig_enemy->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_cloth.png"));

	}break;

	case 3:
	{
			  fig_enemy->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_shear.png"));

	}break;

	}

	if (fig_my->isFlippedX() == false)
	{
		SimpleAudioEngine::getInstance()->playEffect("FingerGuess_lose_huanhusheng.mp3");
		this->runAction(Sequence::create(DelayTime::create(2.0),
			CallFunc::create([=]{
			showLose();
		}), nullptr));

	}
	else
	{
		SimpleAudioEngine::getInstance()->playEffect("FingerGuess_win_huanhusheng.mp3");
		this->runAction(Sequence::create(DelayTime::create(2.0),
			CallFunc::create([=]{
			showWin();
		}), nullptr));

	}


}





void scene_22::call_fig_1(Ref*p)
{

	SimpleAudioEngine::getInstance()->playEffect("show.mp3");
	fig_my->stopAllActions();
	fig_my->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_shear.png"));
	fig_1->setEnabled(false);
	fig_2->setEnabled(false);
	fig_3->setEnabled(false);
	enter->setEnabled(true);
	who = 1;
}


void scene_22::call_fig_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("show.mp3");
	fig_my->stopAllActions();
	fig_my->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_stone.png"));
	fig_1->setEnabled(false);
	fig_2->setEnabled(false);
	fig_3->setEnabled(false);
	enter->setEnabled(true);
	who = 2;
}


void scene_22::call_fig_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("show.mp3");
	fig_my->stopAllActions();
	fig_my->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("FingerGuess_BIG_cloth.png"));
	fig_1->setEnabled(false);
	fig_2->setEnabled(false);
	fig_3->setEnabled(false);
	enter->setEnabled(true);
	who = 3;
}







Scene*scene_23::createScene()
{
	auto scene = Scene::create();
	auto layer = scene_23::create();
	scene->addChild(layer);
	return scene;

}


bool scene_23::init()
{
	Layer::init();

	SpriteFrameCache::getInstance()->removeSpriteFrames();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Jump_Texture.plist");

	auto bg = Sprite::createWithSpriteFrameName("Jump_BG.png");
	bg->setPosition(1136 / 2, 640 / 2);
	this->addChild(bg);


	auto fire = Sprite::createWithSpriteFrameName("fire_1.png");

	fire->setPosition(1136 / 2 - 305, 640 / 2 + 130);


	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("fire_2.png"));

	ani1->setDelayPerUnit(3.0 / 20);
	ani1->setLoops(2);
	ani1->setRestoreOriginalFrame(true);
	fire->runAction(RepeatForever::create(Animate::create(ani1)));



	tizi = Sprite::createWithSpriteFrameName("ladder.png");
	this->addChild(tizi);
	tizi->setPosition(1136 / 2 - 258, 640 / 2 - 115);

	man = Sprite::createWithSpriteFrameName("Master_nervous.png");
	man->setPosition(1136 / 2 - 210, 640 / 2 + 135);
	this->addChild(man);

	auto ani = Animation::create();
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_nervous.png"));
	ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_help.png"));

	ani->setDelayPerUnit(5.0 / 20);
	ani->setLoops(2);
	ani->setRestoreOriginalFrame(true);
	man->runAction(RepeatForever::create(Animate::create(ani)));




	this->addChild(fire);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		beg = target->convertToNodeSpace(touch->getLocation());

		if (man->getTag() == 4)return false;

		if (abs(beg.x - tizi->getPositionX()) < 107 && abs(beg.y - tizi->getPositionY()) < 213)
		{
			return true;
		}


		if ((beg.x >630 && beg.x<856) && (beg.y >50 && beg.y<200))
		{

			man->setTag(4);//jump
			man->stopAllActions();

			man->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_nervous.png"));

			if (tizi->getPositionY()>37)//die
			{
				LEN = 0.3;
				this->scheduleUpdate();
				tizi->setTag(4);//die
				man->runAction(MoveBy::create(1.4, Vec2(380, 0)));
				this->runAction(Sequence::create(DelayTime::create(3.0),
					CallFunc::create([=]{
					showLose();
				}), nullptr));

			}
			else//win
			{
				tizi->setTag(5);//win
				this->scheduleUpdate();
				man->runAction(MoveBy::create(0.9, Vec2(380, 0)));
				LEN = 0.43;

				this->runAction(Sequence::create(DelayTime::create(3.0),
					CallFunc::create([=]{
					showWin();
				}), nullptr));

			}
			return false;

		}

		if (abs(beg.x - man->getPositionX()) < 30 && abs(beg.y - man->getPositionY()) < 63)
		{


			man->setTag(4);//jump
			man->stopAllActions();

			man->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_nervous.png"));

			if (tizi->getPositionY()>37)//die
			{
				LEN = 0.3;
				this->scheduleUpdate();
				tizi->setTag(4);//die
				man->runAction(MoveBy::create(1.3, Vec2(380, 0)));
				SimpleAudioEngine::getInstance()->playEffect("Jump_of_sound.mp3");
				this->runAction(Sequence::create(DelayTime::create(3.0),
					CallFunc::create([=]{
					showLose();
				}), nullptr));

			}
			else//win
			{
				tizi->setTag(5);//win
				this->scheduleUpdate();
				man->runAction(MoveBy::create(0.9, Vec2(380, 0)));
				LEN = 0.43;
				SimpleAudioEngine::getInstance()->playEffect("bridge_jump.mp3");
				man->runAction(Sequence::create(DelayTime::create(0.9), CallFunc::create([=]{
				
					SimpleAudioEngine::getInstance()->playEffect("Jump_yes.mp3");
				}),nullptr));


				this->runAction(Sequence::create(DelayTime::create(3.0),
					CallFunc::create([=]{
					showWin();
				}), nullptr));

			}

			return false;
		}


		return false;
	};

	listener->onTouchMoved = [=](Touch*touch, Event*event)
	{
		auto target = event->getCurrentTarget();
		auto point = target->convertToNodeSpace(touch->getLocation());

		if ((tizi->getPositionY() + (point.y - beg.y) < 200) &&
			(tizi->getPositionY() + (point.y - beg.y)>-90))
		{

			tizi->setPositionY(tizi->getPositionY() + (point.y - beg.y));
			man->setPositionY(man->getPositionY() + (point.y - beg.y));
			log("%f", tizi->getPositionY());
		}
		beg = point;

	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->addChild(publicMenu::createNew());
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Jump_BG.mp3");

	SimpleAudioEngine::getInstance()->playBackgroundMusic("Jump_BG.mp3", true);

	return true;
}





void scene_23::showWin()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_win.mp3");

	_eventDispatcher->setEnabled(true);
	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_23::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(-200, 640 / 2));
	this->addChild(sta);
	sta->runAction(MoveTo::create(0.2, Vec2(1136 / 2 - 200, 640 / 2)));

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-2.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-3.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-4.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-5.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-6.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("win-7.png"));

	ani1->setDelayPerUnit(2.8 / 40);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	auto sta1 = Sprite::create("0048.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_23::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_23::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);


	auto menu_111 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("next-1.png"),
		Sprite::createWithSpriteFrameName("next-2.png"),
		nullptr
		, this, menu_selector(scene_23::call_3));

	menu_111->setPosition(Vec2(1136 / 2 + 140 + 140, 640 / 2 - 50));
	auto menuu111 = Menu::create(menu_111, nullptr);
	menuu111->setPosition(Point::ZERO);
	this->addChild(menuu111);


}



void scene_23::showLose()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("common_ failure.mp3");

	auto menu_ = MenuItemImage::create("no.png", "no.png", "no.png", this, menu_selector(scene_23::call_));
	menu_->setPosition(Vec2(1136 / 2, 640 / 2));
	menu_->setOpacity(99);
	auto menuu = Menu::create(menu_, nullptr);
	menuu->setPosition(Point::ZERO);
	this->addChild(menuu);

	auto bg = Sprite::createWithSpriteFrameName("bg.png");
	bg->setPosition(Vec2(1136 / 2, 640 / 2));
	this->addChild(bg);


	auto sta = Sprite::createWithSpriteFrameName("win-1.png");
	sta->setPosition(Vec2(1136 / 2 - 200, 640 / 2));
	this->addChild(sta);

	auto ani1 = Animation::create();
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-1.png"));
	ani1->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lose-cry-2.png"));

	ani1->setDelayPerUnit(2.8 / 20);
	ani1->setLoops(100);
	ani1->setRestoreOriginalFrame(true);
	sta->runAction(RepeatForever::create(Animate::create(ani1)));

	sta->runAction(JumpBy::create(1.3, Vec2(0, 50), 50, 3));

	auto sta1 = Sprite::create("0047.png");
	sta1->setPosition(Vec2(1136 / 2 + 120, 640 / 2 + 85));
	this->addChild(sta1);


	auto menu_1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("win-lose-list-1.png"),
		Sprite::createWithSpriteFrameName("win-lose-list-2.png"),
		nullptr
		, this, menu_selector(scene_23::call_1));

	menu_1->setPosition(Vec2(1136 / 2, 640 / 2 - 50));
	auto menuu1 = Menu::create(menu_1, nullptr);
	menuu1->setPosition(Point::ZERO);
	this->addChild(menuu1);


	auto menu_11 = MenuItemSprite::create(Sprite::createWithSpriteFrameName("restart-1.png"),
		Sprite::createWithSpriteFrameName("restart-2.png"),
		nullptr
		, this, menu_selector(scene_23::call_2));

	menu_11->setPosition(Vec2(1136 / 2 + 140, 640 / 2 - 50));
	auto menuu11 = Menu::create(menu_11, nullptr);
	menuu11->setPosition(Point::ZERO);
	this->addChild(menuu11);

}





void scene_23::call_1(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(selectScene::createScene());

}

void scene_23::call_2(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(scene_23::createScene());
}

void scene_23::call_3(Ref*p)
{
	SimpleAudioEngine::getInstance()->playEffect("common_click.mp3");

	Director::getInstance()->replaceScene(HelloWorld::createScene());

}




void scene_23::update(float delta)
{

	//man->getPositionY() - 155;

	if (man->getPositionY() <= 155)
	{
		this->unscheduleUpdate();

		if (tizi->getTag() == 4)
		{
			man->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_Died.png"));
		}
		else
		{
			auto ani = Animation::create();
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_happly.png"));
			ani->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("Master_happly_1.png"));
			ani->setDelayPerUnit(5.0 / 20);
			ani->setLoops(2);
			ani->setRestoreOriginalFrame(true);
			man->runAction(RepeatForever::create(Animate::create(ani)));

		}

	}

	man->setPositionY(man->getPositionY() + x);
	x -= LEN;

}



/*end of each game scene
*/










