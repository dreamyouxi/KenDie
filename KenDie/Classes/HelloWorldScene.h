#include "cocos2d.h"
using namespace cocos2d;

#include "network/WebSocket.h"
using namespace  network;









class logo : public Layer
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	CREATE_FUNC(logo);
};

















class notify : public Layer, public WebSocket::Delegate
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();

	Label*label;

	virtual void onOpen(WebSocket* ws)
	{

	}
	virtual void onMessage(WebSocket* ws, const WebSocket::Data& data)
	{
		std::string str = data.bytes;
		back_1->setVisible(true);
		log("%s", str.c_str());
		label->setString(str);
		ws_client->close();
	}
	virtual void onClose(WebSocket* ws)
	{

	}
	virtual void onError(WebSocket* ws, const WebSocket::ErrorCode& error)
	{
		auto dic = Dictionary::createWithContentsOfFile("string.plist");
		back_1->setVisible(true);
		label->setString(((String*)dic->objectForKey("error"))->getCString());
		ws_client->close();
		
	}


	WebSocket* ws_client;

	void cal(Ref*p){}
	MenuItemSprite*bbb;
	MenuItemSprite*back_1;

	void call1(Ref*p);
	CREATE_FUNC(notify);
};









class HelloWorld : public cocos2d::Layer//, public WebSocket::Delegate
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void call(cocos2d::Ref* p);

	void call_info(Ref*p);

	void call_good(Ref*p);

	void call_review(Ref*p);

	MenuItemSprite*musi_of;
	MenuItemSprite*musi_onn;

	void musi_off(Ref*p);

	void musi_on(Ref*p);



	CREATE_FUNC(HelloWorld);
};





class publicMenu :public Layer
{
public:
	static publicMenu*createNew();
	publicMenu();
	void call_select(Ref*p);

	

};




/*start of each game scene
*/


class scene_1 :public Layer
{
public:
	static Scene* createScene();

	virtual bool init();
	Sprite * hero;
	Sprite*  sta_1;
	void showWin();
	void showLose();
	void call_(Ref*p){}

	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	CREATE_FUNC(scene_1);

};



class scene_2 :public Layer
{
public:
	static Scene* createScene();

	virtual bool init();

	void showWin();
	void showLose();
	Sprite* bg;
	void call_(Ref*p){}

	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	int i = 100;
	Label*lab;

	MenuItemSprite*menu_11;
	MenuItemSprite*menu_1;
	void call_red(Ref*p);
	Sprite*ss;
	void call_lo(Ref*p);

	CREATE_FUNC(scene_2);

};



class scene_3 :public Layer
{
public:
	static Scene* createScene();
	bool tag = false;
	bool tagg = false;
	virtual bool init();
	Sprite*tank;
	Sprite*man;
	Sprite*bridge;
	void showWin();
	void showLose();
	void call_(Ref*p){}

	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	virtual void update(float delta);
	int tag1 = false;
	Sprite*mm1;
	void call_11(float p);

	MenuItemSprite*menu_man_sp;
	MenuItemSprite*menu_tank_sp;

	void call_man_1(Ref*p);

	void call_tank(Ref*p);
	MenuItemSprite*walk;
	void call_walk(Ref*p);
	Sprite*mm;
	CREATE_FUNC(scene_3);

};





class scene_4 :public Layer
{
public:
	static Scene* createScene();

	virtual bool init();

	void showWin(Ref*p);
	void showLose();
	MenuItemSprite*menu_11;
	MenuItemSprite*menu_1;
	void call_(Ref*p){}

	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);
	Sprite*ss;
	void call_lose(Ref*p);

	CREATE_FUNC(scene_4);

};


class scene_5 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();

	void showWin();
	void showLose();
	Sprite*hero;
	Point begain;
	Point move;
	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);
	Sprite*bullet[20];
	Sprite*enemy[200];
	int i = 0, mov = 0;
	int ii = 0;
	float dua = 100;
	int x, y;
	virtual void update(float a);

	CREATE_FUNC(scene_5);

};






class scene_6 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();

	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);
	int length = 90;
	Sprite*apple;
	virtual void update(float delta);
	void call_button(Ref*p);


	Sprite*hand;
	Sprite*man;
	MenuItemSprite*menu_;
	Sprite*rope;
	CREATE_FUNC(scene_6);

};





class scene_7 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();

	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);
	Point beg;
	virtual void update(float delta);
	void call_car(Ref*p);

	Sprite*boom_;
	bool isBoom = false;
	bool run = false;

	Sprite*boom;//炸弹
	MenuItemSprite*car;//车
	Sprite*wood;//木头

	CREATE_FUNC(scene_7);

};




class scene_8 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	MenuItemSprite*wu;
	Sprite*lab;
	Sprite*man;

	void call_wu(Ref*p);

	CREATE_FUNC(scene_8);

};






class scene_9 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	//	Sprite*hole[6];

	void call_tap(Ref*p);


	void aaaa(Ref*p);

	MenuItemSprite*stone;
	MenuItemSprite*man;
	MenuItemSprite*hole[6];

	Sprite*bigRock[6];

	bool isRock = false;

	void call_hole_1(Ref*p);
	void call_hole_2(Ref*p);
	void call_hole_3(Ref*p);
	void call_hole_4(Ref*p);
	void call_hole_5(Ref*p);
	void call_hole_6(Ref*p);
	int no = 0;
	void randManPos(Ref*p);

	int manPos = 0;//人位置

	void call_rock(Ref*p);

	CREATE_FUNC(scene_9);

};





class scene_10 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	MenuItemSprite*man_head;
	MenuItemSprite*man;

	MenuItemSprite*bad_head;
	MenuItemSprite*bad;
	bool isManHead = false;
	void call_man_head(Ref*p);
	void call_man(Ref*p);
	void call_bad(Ref*p);
	void call_bad_head(Ref*p);

	CREATE_FUNC(scene_10);

};






class scene_11 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();
	float y = 0;
	int tmp = 0;
	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);
	char str[20];
	Sprite*car;
	Sprite*enemy[10];
	int i = 0;
	bool tag = false;
	virtual void update(float delta);

	CREATE_FUNC(scene_11);

};






class scene_12 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	Sprite*bg;
	void call_bg(Ref*p);
	Sprite*sun;

	MenuItemSprite*ghost;
	Sprite*man_sp;
	Sprite*ghost_sp;
	MenuItemSprite*man;

	Sprite*clould;

	void call_man(Ref*p);
	void call_ghost(Ref*p);

	CREATE_FUNC(scene_12);

};





class scene_13 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	Sprite*dog;
	Sprite*man;

	Sprite*bg, *bg2, *bg3;
	Sprite*bg1;
	int no = 0;
	virtual void update(float delta);

	MenuItemSprite*back;
	void call_back(Ref*p);

	bool isBack = false;
	CREATE_FUNC(scene_13);

};





class scene_14 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	Sprite*win;
	Sprite*boat;
	Sprite*flag[5];
	virtual void update(float delta);
	CREATE_FUNC(scene_14);

};







class scene_15 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	Sprite*bg, *bgg, *man, *sun, *moon, *lab1, *lab2;

	CREATE_FUNC(scene_15);

};







class scene_16 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();
	void showOver();
	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	Layer*lay1, *lay2;
	MenuItemSprite*word[10];
	Sprite*txt[10];// 最多输入10个字
	//int txtLen = 0;

	void call_enter(Ref*p);
	void call_help(Ref*p);
	void call_pass(Ref*p);
	void call_over(Ref*p);

	void call_word_1(Ref*p);
	void call_word_2(Ref*p);
	void call_word_3(Ref*p);
	void call_word_4(Ref*p);
	void call_word_5(Ref*p);
	void call_word_6(Ref*p);
	void call_word_7(Ref*p);
	void call_word_8(Ref*p);
	void call_word_9(Ref*p);
	void call_word_10(Ref*p);

	CREATE_FUNC(scene_16);

};





class scene_17 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	Sprite *boom;
	Sprite *bubble[9][5];
	int no = 0;

	Sprite*paper;

	CREATE_FUNC(scene_17);

};







class scene_18 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);
	virtual void update(float delta);

	Sprite*menu;
	MenuItemSprite*letFly;
	Sprite*gun;

	void call_letFly(Ref*p);

	Sprite*man;
	Sprite*bullet;

	CREATE_FUNC(scene_18);

};





class scene_19 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	int no = 0;//填入的字数 最大4 最小1
	Label*Num[12], *lab[4];

	CREATE_FUNC(scene_19);

};







class scene_20 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);
	Sprite*knife;
	Sprite*man;
	Sprite*bad;

	Sprite*police;
	int a = 0;
	int aa = 0;
	int kk = 0;
	int nowdate = 0;
	virtual void update(float delta);

	void showPolice();
	CREATE_FUNC(scene_20);

};







class scene_21 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();
	int gra_x = 0;
	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	Sprite* bg;
	Sprite*car;
	Sprite*rock;
	MenuItemSprite*pause;
	void call_pause(Ref*p);

	virtual void update(float delta);

	CREATE_FUNC(scene_21);

};








class scene_22 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);

	MenuItemSprite*fig_1;
	int who = 0;

	void call_fig_1(Ref*p);

	void call_fig_2(Ref*p);

	void call_fig_3(Ref*p);


	MenuItemSprite*fig_2;
	MenuItemSprite*fig_3;


	Sprite*fig_my;
	Sprite*fig_enemy;

	MenuItemSprite*enter;

	void call_enter(Ref*p);


	CREATE_FUNC(scene_22);

};








class scene_23 :public Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void showWin();
	void showLose();

	void call_(Ref*p){}
	void call_1(Ref*p);
	void call_2(Ref*p);
	void call_3(Ref*p);
	Point beg,nowp;
	Sprite*tizi;
	Sprite*man;
	float x = 10;
	float LEN = 0;
	virtual void update(float delta);

	CREATE_FUNC(scene_23);

};


/*end of each game scene
*/




typedef struct 
{
	int x;
	int y;
} Point1;







class selectScene :public Layer
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void call(Ref* p);
	Point beg;
	Layer*layer[6];

	void call_scene_1(Ref*p)
	{
		Director::getInstance()->replaceScene(scene_1::createScene());
	}

	Point1 pot[4];//常量四个点 
	Point1 nowp;

	void setLayerVis(int ii)//设置可见layer
	{
		for (int i = 0;i<6;i++)
		{
			layer[i]->setVisible(false);
			star[i]->setVisible(false);
		}
		layer[ii]->setVisible(true);
		star[ii]->setVisible(true);
		UserDefault::getInstance()->setIntegerForKey("selectLayer", ii);
	}

	int getVisibleLayer()//获取当前可见layer
	{
		int i = 0;
		for ( i = 0; i < 6; i++)
		{
			if (layer[i]->isVisible() == true)
				break;
		}
		return i;

	}

	Sprite*star[6];


	Sprite *item[24];
	CREATE_FUNC(selectScene);

};



