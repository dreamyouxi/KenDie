
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({ port: 3011 });

console.log('notifySrv  running on  port 3011');

var sql = require('sqlite3');
var db = new sql.Database('Notify.db');

db.run("CREATE TABLE IF NOT EXISTS notify(MSG TEXT PRIMARY KEY)");
db.close();


wss.on('connection', function (ws) {
    var db = new sql.Database('Notify.db');
    db.all("SELECT MSG FROM notify", function (err, res) {  
                    res.forEach(function (ress) {
                       
                            console.log(ress.MSG);
                            ws.send(ress.MSG);

                            db.close();        
                            ws.close();               
                       });
}
                    );                                                     
});
